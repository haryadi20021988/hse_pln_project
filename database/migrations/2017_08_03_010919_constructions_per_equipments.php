<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConstructionsPerEquipments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('equipercons', function (Blueprint $table) {
          $table->uuid('equipercons_id');
          $table->primary('equipercons_id');
          $table->uuid('equipercons_construction_id');
          $table->uuid('equipercons_equipment_id');
          $table->double('equipercons_cost', 12, 2);
          $table->double('equipercons_use_time', 10, 2);
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('equipercons');
    }
}
