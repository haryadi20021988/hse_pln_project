<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::table('matpercons', function($table) {
         $table->foreign('matpercons_construction_id')->references('construction_id')->on('constructions');
         $table->foreign('matpercons_material_id')->references('material_id')->on('materials');
      });

      Schema::table('equipercons', function($table) {
         $table->foreign('equipercons_construction_id')->references('construction_id')->on('constructions');
         $table->foreign('equipercons_equipment_id')->references('equipment_id')->on('equipments');
      });

      Schema::table('manpercons', function($table) {
         $table->foreign('manpercons_construction_id')->references('construction_id')->on('constructions');
         $table->foreign('manpercons_manpower_id')->references('manpower_id')->on('manpowers');
      });
      
       Schema::table('manpowers', function($table) {
         $table->foreign('manpower_mrs_id')->references('mrs_id')->on('minimum_regional_salary');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

      Schema::table('matpercons', function($table) {
        $table->dropForeign('matpercons_matpercons_construction_id_foreign');
        $table->dropForeign('matpercons_matpercons_material_id_foreign');
      });

      Schema::table('equipercons', function($table) {
        $table->dropForeign('equipercons_equipercons_construction_id_foreign');
        $table->dropForeign('equipercons_equipercons_equipment_id_foreign');
      });

      Schema::table('manpercons', function($table) {
        $table->dropForeign('manpercons_manpercons_construction_id_foreign');
        $table->dropForeign('manpercons_manpercons_manpower_id_foreign');
      });
      
      Schema::table('manpowers', function($table) {
        $table->dropForeign('manpowers_manpower_mrs_id_foreign');
      });

    }
}
