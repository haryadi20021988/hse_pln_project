<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Equipments extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('equipments', function (Blueprint $table) {
            $table->uuid('equipment_id');
            $table->primary('equipment_id');
            $table->string('equipment_type', 75);
            $table->unsignedInteger('equipment_price');
            $table->unsignedTinyInteger('equipment_lifetime');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('equipments');
    }

}
