
-- Equipment Cost

DROP FUNCTION IF EXISTS EquipCost;

DELIMITER $$
 
CREATE FUNCTION EquipCost(price DOUBLE, lifetime DOUBLE) RETURNS DOUBLE
    DETERMINISTIC
BEGIN

    -- Variable initialization
    SET @ir = 0.58 / 100;

    SET @d1 = @ir * POW((1 + @ir), lifetime);
    SET @d2 = POW((1 + @ir), lifetime) - 1;
    SET @result = price * (@d1 / @d2) / (22 * 7);

 RETURN ROUND(@result,2);
END

-- Allowances

DROP FUNCTION IF EXISTS Allowances;

DELIMITER $$
 
CREATE FUNCTION Allowances() RETURNS DOUBLE
    DETERMINISTIC
BEGIN

DECLARE done INT DEFAULT FALSE;
DECLARE t_allowance_value DOUBLE;
DECLARE cur_allowances CURSOR FOR (SELECT allowance_value FROM allowances);
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

SET @total_allowances = 0;

OPEN cur_allowances;

read_loop: LOOP

FETCH cur_allowances INTO t_allowance_value;

IF done THEN
  LEAVE read_loop;
END IF;

SET @d = (SELECT t_allowance_value);
SET @total_allowances = @total_allowances + (@d / 100);

END LOOP;

CLOSE cur_allowances;


RETURN @total_allowances + 1;

END

-- Manpower Cost Montly

DROP FUNCTION IF EXISTS ManpowerCost;

DELIMITER $$
 
CREATE FUNCTION ManpowerCost(coefisien DOUBLE) RETURNS DOUBLE
    DETERMINISTIC
BEGIN

    -- Variable initialization
    SET @allowance = (SELECT Allowances());
    SET @umk = (SELECT mrs_nominal FROM minimum_regional_salary WHERE mrs_status = 1);
    SET @operand = @umk * coefisien;
    SET @operand = @operand * @allowance;


 RETURN @operand;

END

-- Manpower cost hourly
DROP FUNCTION IF EXISTS ManpowerHourlyCost;

DELIMITER $$
 
CREATE FUNCTION ManpowerHourlyCost(coefisien DOUBLE) RETURNS DOUBLE
    DETERMINISTIC
BEGIN

    -- Variable initialization
    SET @manpower = (SELECT ManpowerCost(coefisien));
    SET @result = @manpower / 22;
    SET @result = @result / 7;
    
 RETURN ROUND(@result, 1);

END

-- Calculation to total equipment cost

DROP FUNCTION IF EXISTS TotalEquipmentCost;

DELIMITER $$
 
CREATE FUNCTION TotalEquipmentCost(t_construction_id CHAR(36)) RETURNS DOUBLE
    DETERMINISTIC
BEGIN

SET @result = (SELECT SUM(equipcost(equipments.equipment_price, equipments.equipment_lifetime) * equipercons.equipercons_use_time) as temp_equipercons_total FROM equipercons JOIN equipments ON equipercons.equipercons_equipment_id = equipments.equipment_id WHERE equipercons.equipercons_construction_id = t_construction_id GROUP BY equipercons.equipercons_construction_id);

RETURN ROUND(@result,2);

END

-- Calculation to total time materials
DROP FUNCTION IF EXISTS TotalMaterialTime;

DELIMITER $$
 
CREATE FUNCTION TotalMaterialTime(t_construction_id CHAR(36)) RETURNS DOUBLE
    DETERMINISTIC
BEGIN

SET @result = (SELECT 
SUM(materials.material_setup_time * matpercons.matpercons_quantity) as temp_matpercons_total
FROM matpercons 
JOIN materials ON matpercons.matpercons_material_id = materials.material_id
WHERE matpercons_construction_id = t_construction_id
GROUP BY matpercons.matpercons_construction_id);

RETURN ROUND(@result,2);

END

-- Calculation to total manpower 
ROP FUNCTION IF EXISTS TotalManpowerCost;

DELIMITER $$
 
CREATE FUNCTION TotalManpowerCost(t_construction_id CHAR(36)) RETURNS DOUBLE
    DETERMINISTIC
BEGIN

SET @result = (SELECT 
SUM(ManpowerHourlyCost(manpowers.manpower_coeffisien) *manpercons.manpercons_coefficient * manpercons.manpercons_work_time) as total_manpower_cost
FROM manpercons
JOIN manpowers ON manpercons.manpercons_manpower_id = manpowers.manpower_id
WHERE manpercons.manpercons_construction_id = t_construction_id
GROUP BY manpercons.manpercons_construction_id);

RETURN ROUND(@result,2);

END

-- Get total constructions
SELECT
constructions.construction_name,
TotalEquipmentCost(constructions.construction_id) as equipment_cost,
TotalMaterialTime(constructions.construction_id) as wpm,
TotalManpowerCost(constructions.construction_id) as manpower_cost,
(TotalEquipmentCost(constructions.construction_id) + TotalManpowerCost(constructions.construction_id)) * TotalMaterialTime(constructions.construction_id) as total_cost
FROM constructions;