<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Constructions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('constructions', function (Blueprint $table) {
          $table->uuid('construction_id');
          $table->primary('construction_id');
          $table->string('construction_name', 100);
          $table->timestamps();
          $table->softDeletes();
      });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('constructions');
    }
    
}
