use db_plnconstruction;

-- Equipment search process

SELECT 
equipercons.equipercons_construction_id,
equipments.equipment_price as x,
equipments.equipment_lifetime as y,
equipercons.equipercons_use_time,
equipcost(equipments.equipment_price, equipments.equipment_lifetime) * equipercons.equipercons_use_time  as d
FROM equipercons
JOIN equipments 
ON equipercons.equipercons_equipment_id = equipments.equipment_id
where  equipercons.equipercons_construction_id = '2add8450-9373-11e7-b8bd-d3185d735902';

SELECT 
equipercons.equipercons_construction_id,
SUM(equipments.equipment_price) as x,
SUM(equipments.equipment_lifetime) as y,
SUM(equipercons.equipercons_use_time) as v,
SUM(equipcost(equipments.equipment_price, equipments.equipment_lifetime) * equipercons.equipercons_use_time) as d
FROM equipercons
JOIN equipments 
ON equipercons.equipercons_equipment_id = equipments.equipment_id
GROUP BY equipercons.equipercons_construction_id;

-- Final Equipment per Construction

SELECT 
equipercons.equipercons_construction_id as temp_equipercons_construction_id,
SUM(equipcost(equipments.equipment_price, equipments.equipment_lifetime) * equipercons.equipercons_use_time) as temp_equipercons_total
FROM equipercons
JOIN equipments 
ON equipercons.equipercons_equipment_id = equipments.equipment_id
GROUP BY equipercons.equipercons_construction_id;

-- Process query materials 
SELECT 
matpercons.matpercons_construction_id,
sum(materials.material_setup_time) as x,
sum(matpercons.matpercons_quantity) as u,
sum(materials.material_setup_time * matpercons.matpercons_quantity) as y
FROM matpercons 
JOIN materials 
ON matpercons.matpercons_material_id = materials.material_id
GROUP BY matpercons.matpercons_construction_id;

SELECT 
matpercons.matpercons_construction_id,
materials.material_setup_time,
matpercons.matpercons_quantity,
materials.material_setup_time * matpercons.matpercons_quantity as y
FROM matpercons 
JOIN materials 
ON matpercons.matpercons_material_id = materials.material_id
WHERE matpercons.matpercons_construction_id = '3c15e760-848c-11e7-9a89-9f0209b5080a';

-- Final Materials
SELECT 
matpercons.matpercons_construction_id as temp_matpercons_construction_id,
SUM(materials.material_setup_time * matpercons.matpercons_quantity) as temp_matpercons_total
FROM matpercons 
JOIN materials ON matpercons.matpercons_material_id = materials.material_id
GROUP BY matpercons.matpercons_construction_id;

-- Manpower  search process

use db_plnconstruction;

SELECT 
manpercons.manpercons_construction_id,
manpowers.manpower_qualification,
manpowers.manpower_coeffisien,
manpercons.manpercons_coefficient,
manpercons.manpercons_work_time,
ManpowerHourlyCost(manpowers.manpower_coeffisien) as salary_perhour
FROM manpercons
JOIN manpowers ON manpercons.manpercons_manpower_id = manpowers.manpower_id
WHERE manpercons.manpercons_construction_id = '456a2310-848c-11e7-82a1-f9896f96aa08';

SELECT 
manpercons.manpercons_construction_id,
manpowers.manpower_qualification,
manpowers.manpower_coeffisien,
manpercons.manpercons_coefficient,
manpercons.manpercons_work_time,
ManpowerCost(manpowers.manpower_coeffisien) as salary_perhour,
(ManpowerCost(manpowers.manpower_coeffisien) *manpercons.manpercons_coefficient * manpercons.manpercons_work_time) as total_manpower_cost
FROM manpercons
JOIN manpowers ON manpercons.manpercons_manpower_id = manpowers.manpower_id;

-- FInal 
SELECT 
manpercons.manpercons_construction_id,
SUM(ManpowerCost(manpowers.manpower_coeffisien) *manpercons.manpercons_coefficient * manpercons.manpercons_work_time) as total_manpower_cost
FROM manpercons
JOIN manpowers ON manpercons.manpercons_manpower_id = manpowers.manpower_id
GROUP BY manpercons.manpercons_construction_id;

