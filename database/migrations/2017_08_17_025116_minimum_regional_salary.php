<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MinimumRegionalSalary extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('minimum_regional_salary', function (Blueprint $table) {

            $table->uuid('mrs_id');
            $table->primary('mrs_id');
            $table->smallInteger('mrs_year');
            $table->double('mrs_nominal', 15, 3);
            $table->tinyinteger('mrs_status');
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('minimum_regional_salary');
    }

}
