<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConstructionsPerManpowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('manpercons', function (Blueprint $table) {
          $table->uuid('manpercons_id');
          $table->primary('manpercons_id');
          $table->uuid('manpercons_construction_id');
          $table->uuid('manpercons_manpower_id');
          $table->double('manpercons_coefficient', 10, 3);
          $table->double('manpercons_salary_perhour', 12, 2);
          $table->double('manpercons_total_cost', 12, 2);
          $table->double('manpercons_work_time', 10, 3);
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manpercons');
    }
}
