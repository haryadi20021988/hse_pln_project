<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConstructionsPerMaterials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::create('matpercons', function (Blueprint $table) {
          $table->uuid('matpercons_id');
          $table->primary('matpercons_id');
          $table->uuid('matpercons_construction_id');
          $table->uuid('matpercons_material_id');
          $table->unsignedInteger('matpercons_quantity');
          $table->timestamps();
          $table->softDeletes();
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('matpercons');
    }

}
