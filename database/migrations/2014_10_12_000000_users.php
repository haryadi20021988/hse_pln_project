<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('user_id');
            $table->primary('user_id');
            $table->string('user_fullname', 50);
            $table->string('user_email', 50)->unique();
            $table->string('user_username', 50)->unique();
            $table->string('user_password', 225);
            $table->string('user_type', 15);
            $table->string('user_nip', 20);
            $table->unsignedTinyInteger('user_status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }

}
