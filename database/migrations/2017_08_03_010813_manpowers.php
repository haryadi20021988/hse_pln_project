<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Manpowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('manpowers', function (Blueprint $table) {
          $table->uuid('manpower_id');
          $table->primary('manpower_id');
          $table->uuid('manpower_mrs_id');
          $table->string('manpower_qualification', 50);
          $table->double('manpower_coeffisien', 7, 2);
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('manpowers');
    }

}
