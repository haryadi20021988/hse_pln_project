<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Allowances extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('allowances', function (Blueprint $table) {
            $table->uuid('allowance_id');
            $table->primary('allowance_id');
            $table->string('allowance_key', 50);
            $table->double('allowance_value', 20, 15);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('allowances');
    }

}
