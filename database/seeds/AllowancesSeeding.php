<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class AllowancesSeeding extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        // BPJS Kesehatan
        DB::table('allowances')->insert([
            'allowance_id' => Uuid::generate(),
            'allowance_key' => 'bpjs_kesehatan',
            'allowance_value' => 5
        ]);

        // Jaminan Kecelakaan Kerja ( JKK )
        DB::table('allowances')->insert([
            'allowance_id' => Uuid::generate(),
            'allowance_key' => 'jkk',
            'allowance_value' => 1.27
        ]);

        // Jaminan Kematian (JK)
        DB::table('allowances')->insert([
            'allowance_id' => Uuid::generate(),
            'allowance_key' => 'jk',
            'allowance_value' => 0.3
        ]);

        // Jaminan Hari Tua (JHT)
        DB::table('allowances')->insert([
            'allowance_id' => Uuid::generate(),
            'allowance_key' => 'jht',
            'allowance_value' => 5.7
        ]);

        // Jaminan Pensiun (JP)
        DB::table('allowances')->insert([
            'allowance_id' => Uuid::generate(),
            'allowance_key' => 'jp',
            'allowance_value' => 5
        ]);

        // Tunjangan Hari Raya (THR)
        DB::table('allowances')->insert([
            'allowance_id' => Uuid::generate(),
            'allowance_key' => 'thr',
            'allowance_value' => 8.33333333333333
        ]);
        
        // Pesangon / DPLK 
        DB::table('allowances')->insert([
            'allowance_id' => Uuid::generate(),
            'allowance_key' => 'dplk',
            'allowance_value' => 9.2
        ]);
        
    }

}
