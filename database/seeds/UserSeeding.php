<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Hash;

class UserSeeding extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        // BPJS Kesehatan
        DB::table('users')->insert([
            'user_id' => Uuid::generate(),
            'user_fullname' => 'Administrator',
            'user_nip' => '000000',
            'user_username' => 'pln.administrator',
            'user_email' => 'pln.administrator@gmail.com',
            'user_password' => Hash::make('pln.administrator'),
            'user_type' => 'master',
            'user_status' => 1
        ]);
        
    }

}
