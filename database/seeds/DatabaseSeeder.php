<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $this->call(AllowancesSeeding::class);
        $this->call(MRSSeeding::class);
        $this->call(UserSeeding::class);
    }

}
