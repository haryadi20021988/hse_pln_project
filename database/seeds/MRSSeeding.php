<?php

use Illuminate\Database\Seeder;

class MRSSeeding extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        // BPJS Kesehatan
        DB::table('minimum_regional_salary')->insert([
            'mrs_id' => Uuid::generate(),
            'mrs_year' => 2017,
            'mrs_nominal' => 2125000,
            'mrs_status' => 1
        ]);
        
    }

}
