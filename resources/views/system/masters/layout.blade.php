<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HSE App</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        @include('system.masters.css')
        @yield('css')

    </head>

    <body class="hold-transition skin-blue-light sidebar-mini">
        
        @include('system.masters.hidden_input')
        @yield('hidden_input')
        
        @yield('modal')
        @include('system.masters.modal.delete')

        <div class="wrapper">

            @include('system.masters.header')
            @include('system.masters.sidebar')

            @yield('content')

            @include('system.masters.footer')

        </div>
        <!-- ./wrapper -->

        @include('system.masters.js')
        @yield('js')

    </body>
</html>