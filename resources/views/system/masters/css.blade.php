<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="<?= url('adminlte/bootstrap/css/bootstrap.min.css'); ?>">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= url('adminlte/plugins/font-awesome/css/font-awesome.min.css') ?>">
<!-- Ionicons -->
<link rel="stylesheet" href="<?= url('adminlte/plugins/ionicons/ionicons.min.css') ?>">
<!-- DataTables -->
<link rel="stylesheet" href="<?= url('adminlte/plugins/datatables/dataTables.bootstrap.css'); ?>">
<!-- Theme style -->
<link rel="stylesheet" href="<?= url('adminlte/dist/css/AdminLTE.min.css'); ?>">
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?= url('adminlte/dist/css/skins/_all-skins.min.css'); ?>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<style type="text/css">
    
    .table-font-modif{
        font-size: 14px;
    }

    .box-header-padding{
        padding-bottom: 10px; 
        padding-top: 15px;
    }
    
    .modal-width{
        width: 70%;
    }
    
    .white-space{
        color: #ffffff;
    }
    
    .span-note{
        color:#fe6201;
        font-weight: bold;
    }
    
    .btn-top-padding{
        margin-top: 24px;
    }
    
</style>
