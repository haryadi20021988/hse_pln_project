<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-ajax-confirmation">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="fa fa-warning"></i>
                    Konfirmasi Hapus Data
                </h4>
            </div>

            <div class="modal-body">
                Apakah Anda yakin untuk menghapus data ini?
            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-times"></i>
                            Tutup
                        </button>
                        <button type="button" class="btn btn-remove-apply btn-danger">
                            <i class="fa fa-trash"></i>
                            Hapus
                        </button>
                    </div>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->