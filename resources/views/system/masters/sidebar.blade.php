<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= url('upload/logo.jpg'); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= session('passport')['user_fullname'] ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">Daftar Menu </li>
            <li <?= Request::path() == 'constructions' ? 'class="active"' : ''; ?> >
                <a href="<?= url('constructions'); ?>">
                    <i class="fa fa-map-signs"></i>
                    <span>Konstruksi</span>
                </a>
            </li>
            <li <?= Request::path() == 'manpowers' ? 'class="active"' : ''; ?> >
                <a href="<?= url('manpowers'); ?>">
                    <i class="fa fa-user-plus"></i>
                    <span>Tenaga Kerja</span>
                </a>
            </li>
            <li <?= Request::path() == 'equipments' ? 'class="active"' : ''; ?> >
                <a href="<?= url('equipments'); ?>">
                    <i class="fa  fa-wrench"></i>
                    <span>Peralatan</span>
                </a>
            </li>
            <li <?= Request::path() == 'materials' ? 'class="active"' : ''; ?> >
                <a href="<?= url('materials'); ?>">
                    <i class="fa  fa-cubes"></i>
                    <span>Material</span>
                </a>
            </li>
            <li class="administrator <?= Request::path() == 'minimum-regional-salary' ? 'active' : ''; ?>" >
                <a href="<?= url('minimum-regional-salary'); ?>">
                    <i class="fa  fa-money"></i>
                    <span>UMR</span>
                </a>
            </li>
            <li class="administrator <?= Request::path() == 'allowances' ? 'active' : ''; ?>"  >
                <a href="<?= url('allowances'); ?>">
                    <i class="fa fa-child"></i>
                    <span>Tunjangan</span>
                </a>
            </li>
            <li class="administrator <?= Request::path() == 'users' ? 'active' : ''; ?>" >
                <a href="<?= url('users'); ?>">
                    <i class="fa  fa-users"></i>
                    <span>Pengguna</span>
                </a>
            </li>
        </ul>

    </section>
    <!-- /.sidebar -->
</aside>