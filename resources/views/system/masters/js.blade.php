<!-- jQuery 2.2.3 -->
<script src="<?= url('adminlte/plugins/jQuery/jquery-2.2.3.min.js'); ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= url('adminlte/bootstrap/js/bootstrap.min.js'); ?>"></script>
<!-- DataTables -->
<script src="<?= url('adminlte/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= url('adminlte/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?= url('adminlte/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?= url('adminlte/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?= url('adminlte/dist/js/app.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= url('adminlte/dist/js/demo.js'); ?>"></script>
<!-- Form Error Alert -->
<script src="<?= url('adminlte/plugins/utility/form-error-alert.js'); ?>"></script>
<!-- UUID generator -->
<script src="<?= url('adminlte/plugins/utility/general.js'); ?>"></script>
<!-- Message error  -->
<script src="<?= url('adminlte/plugins/utility/message-error.js'); ?>"></script>
<!-- Enable manpower calculated -->
<script src="<?= url('adminlte/plugins/utility/manpower.js'); ?>"></script>
<!-- Enable manpower calculated -->
<script src="<?= url('adminlte/plugins/utility/currency.js'); ?>"></script>

<!-- page script -->
<script>

    // Remove alert after 3 second
    setTimeout(function () {
        $('.alert').hide('slow');
    }, 3000);

    // Hidden based on user type
    $(document).ready(function () {

        // Variable initialization
        var user_type = $('input[name="user_type"]').val();

        // Remove element when user is not administrator
        if (user_type !== 'Administrator' && user_type !== 'master') {

            // Do removing 
            $('.administrator').remove();

        }

    });


    // @per : [month, day]
    function manpower_cost(coeffisien) {

        // Variable initialization
        var url = $('input[name="manpower-cost"]').val() + '?';
        var result = null;

        // Add coeffesien value
        url += '&coeffisien=' + coeffisien.toString();

        // Get value from server
        $.get(url).done(function (data) {
            $('input[name="global_var"]').val(data);
        });

        result = $('input[name="global_var"]').val();

        // Return value
        return parseFloat(result);

    }

    // Search action button 
    $('.btn-search').click(function () {

        // Calling searching function
        searching();

    });

    // Key press search action 
    $('input[name="input_search"]').on('keypress', function (e) {

        // Redirect into index page if enter key is press
        if (e.keyCode == 13) {

            // Calling searching function
            searching();

        }

    });

    // Function to build search url
    function searching() {

        // Variable initialization
        var data = $('input[name="input_search"]').val();

        // Build url 
        var url = base_url + '?search=' + data;

        // Redirect into construction index
        window.location = url;

    }

</script>