@if (session('error-alert'))
<div class="alert alert-error alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <?php if (is_array(session('error-alert'))): ?>
        <?php foreach (session('error-alert') as $key => $item): ?>
            <?= $item; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <?= session('error-alert') ?>
    <?php endif; ?>
</div>
@endif