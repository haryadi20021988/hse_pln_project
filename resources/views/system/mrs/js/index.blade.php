<script type="text/javascript">

    // Global variable initialization
    var base_url = $('input[name="base_url"]').val();
    var method_ = '<input type="hidden" name="_method" value="PUT">';
    var error = $('input[name="error"]').val();
    var btn_add = '<button type="submit" class="btn btn-info btn-add"><i class="fa fa-plus"></i> Tambah Data UMR</button>';
    var btn_edit = '<button type="submit" class="btn btn-success btn-update"><i class="fa fa-save"></i> Perbarui Data UMR</button>'
            + '&nbsp;<button type="button" class="btn btn-danger btn-cancel"><i class="fa fa-times"></i> Batal</button>';


    // First initialization 
    $(document).ready(function () {

        // Check is has error
        if (error !== '') {

            // Convert json string format into javascript object 
            var obj = JSON.parse(error);

            // Break down into array list
            $.each(obj, function (index, value) {
                show_alert(index, value);
            });

        }

    });

    // Datatable 
    $("#mrs-table").DataTable({
        "ordering": false,
        "paging": false,
        "searching": false,
        "info": false
    });

    // Edit button action
    $('body').on('click', '.btn-edit', function () {

        // Variable initialiation
        var data = $(this).attr('data-value');

        // Convert data from json string into js object
        data = JSON.parse(data);

        // Append data into form
        $('select[name="mrs_year"]').val(data['mrs_year']).change();
        $('input[name="mrs_nominal"]').val(data['mrs_nominal']);

        // Apend data into form by check mrs-status valeu
        if (data['mrs_status'] == 1) {
            $('.mrs-status-1').prop('checked', true);
        } else {
            $('.mrs-status-0').prop('checked', true);
        }

        // Change button placeholder
        $('.btn-placeholder').empty();
        $('.btn-placeholder').append(btn_edit);

        // Change form action url
        $('.mrs-form').attr('action', base_url + '/' + data['mrs_id']);

        // Append input method_ 
        $('.hidden-input-placehoder').append(method_);

    });

    // Cancel button action
    $('body').on('click', '.btn-cancel', function () {

        // Clear all input form
        $('input[name="mrs_year"]').val('');
        $('input[name="mrs_nominal"]').val('');
        $('.mrs-status-0').prop('checked', true);

        // Clear action form url
        $('.mrs-form').attr('action', base_url);

        // Remove methode hidden input 
        $('.hidden-input-placehoder').find('input[name="_method"]').remove();

        // Change button submit
        $('.btn-placeholder').empty();
        $('.btn-placeholder').append(btn_add);

    });

    // Open delete modal confirmation
    show_modal_delete(base_url);

</script>