@extends('system.masters.layout')

@section('css')
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= url('adminlte/plugins/select2/select2.min.css') ?>">
@stop

@section('hidden_input')
<input type="hidden" name="base_url" value="<?= url('minimum-regional-salary') ?>" />
@stop

@section('modal')

@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

        @include('system.masters.alert.error')
        @include('system.masters.alert.success')

        <div class="box box-solid administrator">
            <div class="box-header with-border">
                <h3 class="box-title">Form Input Data Upah Minimum Regional</h3>
            </div>
            <div class="box-body">
                <form class="mrs-form" action="<?= url('minimum-regional-salary') ?>" method="post" >

                    <div class="hidden-input-placehoder">
                        {!! csrf_field() !!}    
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Tahun</label>
                                <select name="mrs_year" class="form-control select2">
                                    <?php for ($i = 2000; $i < ((int) date('Y') + 5); $i++): ?>
                                        <option value="<?= $i ?>"><?= $i ?></option>
                                    <?php endfor; ?>
                                </select>
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nominal UMR</label>
                                <input 
                                    type="number" 
                                    min="0" 
                                    name="mrs_nominal" 
                                    class="form-control" 
                                    required="true" 
                                    value='<?= old('mrs_nominal'); ?>'
                                    />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Status</label>
                                <div class="checkbox">
                                    <label style="padding-left: 0px;">
                                        <input name="mrs_status" class="mrs-status-1" value="1" type="radio" checked="true" />&nbsp;
                                        Aktif
                                    </label>
                                    <label>
                                        <input name="mrs_status" class="mrs-status-0"  value="0" type="radio" />&nbsp;
                                        Non Aktif
                                    </label>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="btn-placeholder btn-top-padding">
                                    <button type="submit" class="btn btn-info btn-add">
                                        <i class="fa fa-plus"></i> Tambah Data UMR
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border box-header-padding">
                        <h3 class="box-title">Daftar Upah Mininmum Regional</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="mrs-table" class="table table-borderless table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th width="15%" >
                                                Tahun
                                            </th>
                                            <th width="20%" >
                                                Nominal
                                            </th>
                                            <th width="10%" >
                                                Status
                                            </th>
                                            <th width="55%" ></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{$item->mrs_year}}</td>
                                            <td>{{$item->mrs_nominal}}</td>
                                            <td>
                                                <?= $item->mrs_status == 1 ? '<label class="label label-success" >Aktif</label>' : '<label class="label label-danger" >Non-Aktif</label>'; ?>
                                            </td>
                                            <td>
                                                <div class="pull-right administrator">
                                                    <div class="btn-group">
                                                        <button 
                                                            class="btn btn-edit btn-success btn-xs" 
                                                            data-value='<?= json_encode($item); ?>' 
                                                            data-toggle="tooltip" 
                                                            data-placement="top" 
                                                            title="Ubah Data Konstruksi" 
                                                            >
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button 
                                                            class="btn btn-delete btn-danger btn-xs" 
                                                            data-toggle="tooltip" 
                                                            data-placement="top"
                                                            title="Hapus Data Material"
                                                            data-id="{{ $item->mrs_id }}"
                                                            >
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="page-link pull-right">
                                    {{ $data->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer with-border">
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@stop

@section('js')

@include('system.mrs.js.index')

<script src="<?= url('adminlte/plugins/select2/select2.full.min.js'); ?>"></script>

<script type="text/javascript">

// Setup select2 
$('.select2').select2();

</script>


@stop