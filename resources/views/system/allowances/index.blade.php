@extends('system.masters.layout')

@section('modal')

@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

        @include('system.masters.alert.error')
        @include('system.masters.alert.success')

        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border box-header-padding">
                        <h3 class="box-title">Pengaturan Persentase Tunjangan</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <form action="<?= url('allowances') ?>" method="post">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped" style="font-size: 16px;">
                                        <thead>
                                            <tr>
                                                <th width="70%" >Jenis Tunjangan</th>
                                                <th width="30%" style="text-align: right;" >Persentase (%)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    BPJS Kesehatan
                                                </td>
                                                <td class="pull-right">
                                                    <input 
                                                        name="allowance[bpjs_kesehatan]" 
                                                        type="number" 
                                                        step="0.1" 
                                                        class="form-control" 
                                                        value="<?= $data['bpjs_kesehatan']; ?>"
                                                        />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Jaminan Kecelakaan Kerja
                                                </td>
                                                <td class="pull-right">
                                                    <input 
                                                        name="allowance[jkk]" 
                                                        type="number" 
                                                        step="0.1" 
                                                        class="form-control" 
                                                        value="<?= $data['jkk']; ?>"
                                                        />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Jaminan Kematian
                                                </td>
                                                <td class="pull-right">
                                                    <input 
                                                        name="allowance[jk]" 
                                                        type="number" 
                                                        step="0.1" 
                                                        class="form-control"
                                                        value="<?= $data['jk']; ?>"
                                                        />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Jaminan Hari Tua
                                                </td>
                                                <td class="pull-right">
                                                    <input 
                                                        name="allowance[jht]" 
                                                        type="number" 
                                                        step="0.1" 
                                                        class="form-control" 
                                                        value="<?= $data['jht']; ?>"
                                                        />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Jaminan Pensiun
                                                </td>
                                                <td class="pull-right">
                                                    <input 
                                                        name="allowance[jp]" 
                                                        type="number" 
                                                        step="0.1" 
                                                        class="form-control"
                                                        value="<?= $data['jp']; ?>"
                                                        />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Tunjangan Hari Raya
                                                </td>
                                                <td class="pull-right">
                                                    <input 
                                                        name="allowance[thr]" 
                                                        type="number" 
                                                        step="0.00000000000001" 
                                                        class="form-control" 
                                                        value="<?= $data['thr']; ?>"
                                                        />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Pesangon
                                                </td>
                                                <td class="pull-right">
                                                    <input 
                                                        name="allowance[dplk]" 
                                                        type="number" step="0.1" 
                                                        class="form-control"
                                                        value="<?= $data['dplk']; ?>"
                                                        />
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Simpan semua konfigurasi tunjangan?</th>
                                                <th class="pull-right">
                                                    <button class="btn btn-success">
                                                        <i class="fa fa-save"></i> Simpan
                                                    </button>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer with-border">
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@stop

@section('js')

@stop