@extends('system.masters.layout')

@section('hidden_input')
<input type="hidden" name="base_url" value="<?= url('equipments') ?>" />
@stop

@section('modal')

@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

        @include('system.masters.alert.error')
        @include('system.masters.alert.success')

        <div class="box box-solid administrator">
            <div class="box-header with-border">
                <h3 class="box-title">Form Input Data Peralatan</h3>
            </div>
            <div class="box-body">

                <form class="equipments-form" method="post" >

                    <div class="hidden-input-placehoder">
                        {!! csrf_field() !!}    
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Jenis Peralatan</label>
                                <input type="text" name="equipment_type" class="form-control" />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Harga</label>
                                <input type="number" step="0.1" name="equipment_price" class="form-control" />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Masa Pakai (Bulan)</label>
                                <input type="number" step="0.1" name="equipment_lifetime" class="form-control" value="12" readonly="true" />

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="btn-placeholder btn-top-padding">
                                    <button type="submit" class="btn btn-info btn-add">
                                        <i class="fa fa-plus"></i> Tambah Peralatan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border box-header-padding">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="box-title">Daftar Peralatan</h3>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input name="input_search" 
                                           type="text" 
                                           class="form-control" 
                                           placeholder="Masukkan kata kunci.. ." 
                                           aria-label="Search for..."
                                           value="<?= $input_search ?>"
                                           >
                                    <span class="input-group-btn">
                                        <button class="btn btn-search btn-default" type="button" >
                                            <i class="fa fa-search"></i> Cari Data
                                        </button>
                                        <a class="btn btn-default" href="<?= url('equipments') ?>" >
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="equipment-table" class="table table-borderless table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th width="30%" >
                                                Jenis Peralatan
                                            </th>
                                            <th width="20%" >
                                                Harga
                                            </th>
                                            <th width="15%" >
                                                Masa Pakai
                                            </th>
                                            <th width="20%" >
                                                Harga per Jam
                                            </th>
                                            <th width="15%" ></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->equipment_type }}</td>
                                            <td>Rp. {{ number_format($item->equipment_price,0,',','.') }}</td>
                                            <td>{{ $item->equipment_lifetime }} Bulan</td>
                                            <td>Rp. <?= number_format(App\Library\Utility\EquipmentCalc::init($item->equipment_price)->get(2), '2', ',', '.'); ?> / Jam</td>
                                            <td>
                                                <div class="pull-right administrator">
                                                    <div class="btn-group">
                                                        <button class="btn btn-edit btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Ubah Data Konstruksi" data-value='<?= json_encode($item); ?>' >
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button class="btn btn-delete btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus Data Material" data-id='<?= $item->equipment_id; ?>' >
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="page-link pull-right">
                                    {{ $data->appends(['search' => $input_search ])->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer with-border">
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@stop

@section('js')
@include('system.equipments.js.index')
@stop