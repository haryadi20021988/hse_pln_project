<script type="text/javascript">

    // Global variable 
    var base_url = $('input[name="base_url"]').val();
    var detail_manpower_emptyrow = '<tr class="empty-row"><td colspan="5" style="text-align: center;">No Data Available</td></tr>';
    var detail_equipment_emptyrow = '<tr class="empty-row"><td colspan="4" style="text-align: center;">No Data Available</td></tr>';
    var detail_material_emptyrow = '<tr class="empty-row"><td colspan="4" style="text-align: center;">No Data Available</td></tr>';

    // Open new modal
    $('body').on('click', '.btn-add-detail', function () {

        // Get id construction
        var id = $(this).attr('data-id');

        // Initialization title
        $('#modal-detail').find('.modal-title-placeholder').text('Detail Konstruksi');

        // Show modal
        $('#modal-detail').modal();
        
        // Set url to dowload detail
        url_download_detail(id);

        // Ajax load 
        detail_ajax(id);

    });

    // Close modal action
    $('#modal-detail').on('hidden.bs.modal', function (e) {

        // Reset all placeholder into empty condition
        $('#modal-detail').find('.modal-title-placeholder').text('');

        // Clean name construction name
        $('#detail_construction_name').text(' - ');

        // Clean total cost construction
        $('#detail_construction_total_cost').find('span').text(' - ');

        // Clean manpower modal
        detail_manpower_clean();

        // Clean equipemtn modal
        detail_equipment_clean();

        // Clean material modal
        detail_meterial_clean();
        
        // Clean url download detail
        clean_url_download_detail();


    });

    function url_download_detail(id) {

        // Get href link
        var href = $('input[name="detail_constrcution_url"]').val();

        // Add href link with id construction
        href = href + '/' + id;

        // Append href with id into anchor tag
        $('.btn-detail-download').attr('href', href);

    }

    function clean_url_download_detail() {

        // Clean href link on anchor tag
        $('.btn-detail-download').attr('href', '');

    }

    function detail_process(object) {

        // Setup general
        detail_general(object);

        // Manpower break down process
        detail_manpower(object['manpercons']);

        // Equipemt break down process
        detail_equipment(object['equipercons']);

        // Materials break down process
        detail_meterial(object['matpercons']);

    }

    function detail_general(object) {

        // Variable initialization
        var construction_name = object['construction_name'];
        var construction_total_cost = object['total_cost'];

        // Append data into html
        $('#detail_construction_name').text(construction_name);

        if (construction_total_cost > 0) {
            $('#detail_construction_total_cost').find('span').text(formating(construction_total_cost));
        } else {
            $('#detail_construction_total_cost').find('span').text('-');
        }


    }

    // Detail manpower 
    function detail_manpower(object) {

        // Break down data
        if (object.length > 0) {

            // Clear empty row
            $('#detail-manpower-table').find('tbody').empty();

            $.each(object, function (index, value) {

                // Append object into html body
                detail_manpower_html(value);

            });

            // Calculating total manpowers cost
            detail_manpower_calculating(object);

        }



    }

    function detail_manpower_html(object) {

        // Variable initialization
        var total_cost = (object['manpowers']['manpower_hourlycost'] * object['manpercons_coefficient']) * object['manpercons_work_time'];
        var html = '<tr>'
                + '<td>' + object['manpowers']['manpower_qualification'] + '</td>'
                + '<td>' + object['manpercons_coefficient'] + '</td>'
                + '<td class="pull-right">Rp. ' + formating(object['manpowers']['manpower_hourlycost']) + ',-</td>'
                + '<td>' + object['manpercons_work_time'] + ' Jam</td>'
                + '<td class="pull-right" >Rp. ' + formating(total_cost) + ' ,-</td>'
                + '</tr>';

        // Append manpower  
        $('#detail-manpower-table').find('tbody').append(html);

    }

    function detail_manpower_calculating(object) {

        // Variable initialization 
        var result = 0;
        // Break down calculating
        $.each(object, function (index, value) {

            // Get total cost 
            var total_cost = (value['manpowers']['manpower_hourlycost'] * value['manpercons_coefficient']) * value['manpercons_work_time'];
            // Accumulating result
            result += total_cost;

        });

        $('#detail-manpower-total-cost').html(formating(result.toFixed(2)));

    }

    function detail_manpower_clean() {

        // Clean manpower table
        $('#detail-manpower-table').find('tbody').html(detail_manpower_emptyrow);

        // Clean manpower total cost
        $('#detail-manpower-total-cost').text('-');

    }

    // Detail manpower 
    function detail_equipment(object) {

        // Check is not empyt
        if (object.length > 0) {

            // Clear empty row
            $('#detail-equipments-table').find('tbody').empty();

            // Break down data
            $.each(object, function (index, value) {

                // Append object into html body
                detail_equipment_html(value);

            });

            // Calculating total manpowers cost
            detail_equipment_calculating(object);

        }

    }

    function detail_equipment_html(object) {

        // Variable initialization
        var total_equipment_cost = object['equipments']['equipment_cost'] * object['equipercons_use_time'];
        var html = '<tr>'
                + '<td>' + object['equipments']['equipment_type'] + '</td>'
                + '<td class="pull-right">Rp. ' + formating(object['equipments']['equipment_cost']) + ' </td>'
                + '<td>' + object['equipercons_use_time'] + ' Jam</td>'
                + '<td class="pull-right" >Rp. ' + formating(total_equipment_cost) + ' </td>'
                + '</tr>';

        // Append manpower  
        $('#detail-equipments-table').find('tbody').append(html);

    }

    function detail_equipment_calculating(object) {

        // Variable initialization 
        var result = 0;

        // Break down calculating
        $.each(object, function (index, value) {

            // Get total cost 
            var total_equipment_cost = value['equipments']['equipment_cost'] * value['equipercons_use_time'];

            // Accumulating result
            result += total_equipment_cost;

        });

        // Append result into html
        $('#detail-equipment-total-cost').html(formating(result.toFixed(2)));

    }

    function detail_equipment_clean() {

        // Clean manpower table
        $('#detail-equipments-table').find('tbody').html(detail_equipment_emptyrow);

        // Clean manpower total cost
        $('#detail-equipment-total-cost').text('-');

    }

    // Detail materials 
    function detail_meterial(object) {

        // Check is not empyt
        if (object.length > 0) {

            // Clear empty row
            $('#detail-meterial-table').find('tbody').empty();

            // Break down data
            $.each(object, function (index, value) {

                // Append object into html body
                detail_meterial_html(value);

            });

            // Calculating total manpowers cost
            detail_meterial_calculating(object);

        }

    }

    function detail_meterial_html(object) {

        // Variable initialization
        var total_material_cost = (object['materials']['material_setup_time'] * object['matpercons_quantity']);
        var html = '<tr>'
                + '<td>' + object['materials']['material_name'] + '</td>'
                + '<td> ' + object['materials']['material_setup_time'].toFixed(1) + ' Menit </td>'
                + '<td>' + object['matpercons_quantity'] + ' Jam</td>'
                + '<td>' + total_material_cost.toFixed(1) + ' </td>'
                + '</tr>';

        // Append manpower  
        $('#detail-meterial-table').find('tbody').append(html);

    }

    function detail_meterial_calculating(object) {

        // Variable initialization 
        var result = 0;

        // Break down calculating
        $.each(object, function (index, value) {

            // Get total cost 
            var total_material_cost = (value['materials']['material_setup_time'] * value['matpercons_quantity']);

            // Accumulating result
            result += total_material_cost;

        });

        // Convert into hour
        result = result / 60;

        // Append result into html
        $('#detail-meterial-total-cost').html(result.toFixed(2));

    }

    function detail_meterial_clean() {

        // Clean manpower table
        $('#detail-meterial-table').find('tbody').html(detail_material_emptyrow);

        // Clean manpower total cost
        $('#detail-meterial-total-cost').text('-');

    }


    function detail_ajax(id) {

        // Get data from server
        $.get(base_url + '/' + id, function (data) {

            // Break down object
            detail_process(data);

        });

    }

</script>