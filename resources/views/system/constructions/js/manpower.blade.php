<script type="text/javascript">

    // Global variable
    var manpercons_url = $('input[name="manpercons_url"]').val();
    var manpower_deleting = [];
    var manpower_rowid = null;
    var manpower_empty_row = '<tr class="empty-row"><td colspan="6" style="text-align: center;">No Data Available</td></tr>';
    var manpower_btn_add = '<button type="button" class="btn btn-add btn-info"><i class="fa fa-plus"></i> Tambah</button>';
    var manpower_btn_edit = '<button type="button" class="btn btn-update btn-success">'
            + '<i class="fa fa-save"></i> Perbarui'
            + '</button>&nbsp;'
            + '<button type="button" class="btn btn-cancel btn-danger">'
            + '<i class="fa fa-times"></i> Batal'
            + '</button>';

    // Open modal action
    $('#modal-manpower').on('shown.bs.modal', function (e) {

        // Activating select2
        $('.select2').select2();

    });

    // Open new modal
    $('body').on('click', '.btn-add-manpower', function () {

        // Variable initialization
        var id = $(this).attr('data-id');

        // Initialization title
        $('#modal-manpower').find('.modal-title-placeholder').text('Tambah Data Tenaga Kerja');

        // Append construction id
        $('#modal-manpower').find('input[name="manpercons_construction_id"]').val(id);

        // Show modal
        $('#modal-manpower').modal();

        // Synchronize data from serve
        manpower_ajax(id);

    });

    // Close modal action
    $('#modal-manpower').on('hidden.bs.modal', function (e) {

        // Reset all placeholder into empty condition
        $('#modal-manpower').find('.modal-title-placeholder').text('');

        // Remove id construction
        $('#modal-manpower').find('input[name="manpercons_construction_id"]').val('');

        // Clear table
        $('#table-manpercons').find('tbody').html(manpower_empty_row);

    });

    // Add material items
    $('#modal-manpower').on('click', '.btn-add', function () {

        // Form validation
        if (manpower_validation() == false) {
            return false;
        }

        // Variable initialization
        var object = manpower_grab_form();

        // Append data into html
        manpower_html_appending(object);

        // Calculation setup time
        manpower_calculation();

        // Reset form
        manpower_reset_form();

    });

    // Add material items
    $('#modal-manpower').on('click', '.btn-edit', function () {

        // Get data from row table
        var data = $(this).parent().parent().find('input[name="manpower_items[]"]').val();

        // Convert data from string json into javascript object
        data = JSON.parse(data);

        // Get row id 
        manpower_rowid = $(this).attr('data-rowid');

        // Append data into form
        $('input[name="manpercons_id"]').val(data['manpercons_id']);
        $('select[name="manpercons_manpower_id"]').val(data['manpercons_manpower_id']).change();
        $('input[name="manpercons_coefficient"]').val(data['manpercons_coefficient']);
        $('input[name="manpercons_work_time"]').val(data['manpercons_work_time']);

        // Swith button
        $('#modal-manpower').find('.btn-placeholder').html(manpower_btn_edit);

    });

    // Update manpower action
    $('body').on('click', '.btn-update', function () {

        // Form validation
        if (manpower_validation() == false) {
            return false;
        }

        // Variable initializaion
        var object = manpower_grab_form();

        // Append data into html
        manpower_html_appending(object, manpower_rowid);

        // Calculation setup time
        manpower_calculation();

        // Reset form
        manpower_reset_form();

        // Swith button
        $('#modal-manpower').find('.btn-placeholder').html(manpower_btn_add);

    });

    // Cancel button action
    $('#modal-manpower').on('click', '.btn-cancel', function () {

        // Reset form
        manpower_reset_form();

        // Swith button
        $('#modal-manpower').find('.btn-placeholder').html(manpower_btn_add);

    });

    // Delete row
    $('#modal-manpower').on('click', '.btn-remove', function () {

        // Get dom object
        var dom = $(this).parent().parent().parent();

        // Open delete modal confirmation
        $('#modal-delete-ajax-confirmation').modal({
            backdrop: 'static',
            keyboard: false
        });

        manpower_remove_apply(dom);

    });

    // Remove apply
    function manpower_remove_apply(dom) {

        $('#modal-delete-ajax-confirmation').on('click', '.btn-remove-apply', function (event) {

            // Get data 
            var data_ = $(dom).find('input[name="manpower_items[]"]').val();

            // Convert json string into javascript array or object
            data_ = JSON.parse(data_);

            // Append id material into array stack when id is exit
            if (data_['manpercons_id'] !== '') {

                // Add id into array
                manpower_deleting.push(data_['manpercons_id']);

                // Convert array into json string
                var deleting_ = JSON.stringify(manpower_deleting);

                // Append array data into input
                $('#modal-manpower').find('input[name="manpercons_deleted"]').val(deleting_);

            }

            // Remove row
            $(dom).remove();

            // Calculation setup time
            manpower_calculation();

            // Get row length in table
            var row_length = $('#table-manpercons').find('tbody tr').length;

            // Check wheter row is empty or not
            if (row_length == 0) {
                $('#table-manpercons').find('tbody').append(manpower_empty_row);
            }

            // Close delete modal confirmation
            $('#modal-delete-ajax-confirmation').modal('hide');

            // Remove event 
            $("#modal-delete-ajax-confirmation").off("click", ".btn-remove-apply");

        });

    }


    // Save materials item action
    $('#modal-manpower').on('click', '.btn-save', function () {

        // Check is row already exist 
        var row_length = $('input[name="manpower_items[]"]').length;

        // Submit when row length is filled
        if (row_length > 0) {

            // Submit form
            $('.form-manpower').submit();

        } else {

            // Show error alert
            table_alert('Data tenaga kerja masih kosong');

        }

    });

    function manpower_grab_form() {


        // Variable initialization
        var object = {};
        var manpercons_id = $('#modal-manpower').find('input[name="manpercons_id"]').val();
        var manpercons_construction_id = $('#modal-manpower').find('input[name="manpercons_construction_id"]').val();
        var manpercons_manpower_id = $('#modal-manpower').find('select[name="manpercons_manpower_id"] option:selected').val();
        var manpercons_manpower_detail = JSON.parse($('#modal-manpower').find('select[name="manpercons_manpower_id"] option:selected').attr('data-item'));
        var manpercons_coefficient = $('#modal-manpower').find('input[name="manpercons_coefficient"]').val();
        var manpercons_work_time = $('#modal-manpower').find('input[name="manpercons_work_time"]').val();
        var manpercons_salary_perhour = $('#modal-manpower').find('select[name="manpercons_manpower_id"] option:selected').attr('data-hourcost');
        var manpercons_total_cost = manpower_total_cost(manpercons_coefficient, manpercons_work_time, manpercons_salary_perhour);

        // Build data collected
        object.manpercons_id = manpercons_id;
        object.manpercons_construction_id = manpercons_construction_id;
        object.manpercons_manpower_id = manpercons_manpower_id;
        object.manpercons_coefficient = manpercons_coefficient;
        object.manpercons_work_time = manpercons_work_time;
        object.manpercons_salary_perhour = manpercons_salary_perhour;
        object.manpercons_total_cost = Number((manpercons_total_cost).toFixed(1));
        object.manpercons_manpower_detail = manpercons_manpower_detail;

        // Return data as object
        return object;

    }

    function manpower_validation() {

        // Variable initialization
        var is_valid = true;
        var manpercons_manpower_id = $('#modal-manpower').find('select[name="manpercons_manpower_id"] option:selected').val();
        var manpercons_coefficient = $('#modal-manpower').find('input[name="manpercons_coefficient"]').val();
        var manpercons_work_time = $('#modal-manpower').find('input[name="manpercons_work_time"]').val();

        // Clear error validation
        clear_error_message();

        // Check is manpower has selected
        if (manpercons_manpower_id == '') {

            // Get parent dom
            var dom = $('#modal-manpower').find('select[name="manpercons_manpower_id"]');

            // Show error message
            error_message(dom, 'Please select manpower qualification');

            // Set flag
            is_valid = false;

        }

        // Check is quantity has filled
        if (manpercons_coefficient == '') {

            // Get parent dom
            var dom = $('#modal-manpower').find('input[name="manpercons_coefficient"]');

            // Show error message
            error_message(dom, 'Please input manpower coefficient');

            // Set flag
            is_valid = false;

        }

        // Check is quantity has filled
        if (manpercons_work_time == '') {

            // Get parent dom
            var dom = $('#modal-manpower').find('input[name="manpercons_work_time"]');

            // Show error message
            error_message(dom, 'Please input manpower work time');

            // Set flag
            is_valid = false;

        }

        // Return check result
        return is_valid;

    }

    function manpower_html_appending(object, rowid) {

        // Check is update or new
        if (rowid == undefined) {
            if (object.manpercons_id == '') {
                // Remove empty row
                var rowid_ = guid();
            } else {
                var rowid_ = object.manpercons_id;
            }
        } else {
            // Replace with new value
            var rowid_ = rowid;
        }

        // Variable initalization
        var input = "<input type='hidden' name='manpower_items[]' value='" + JSON.stringify(object) + "' />";
        var html = "<tr data-id='" + rowid_ + "' >"
                + "<td>" + object.manpercons_manpower_detail['manpower_qualification'] + "</td>"
                + "<td>" + object.manpercons_coefficient + " </td>"
                + "<td class='pull-right' > Rp. " + formating(object.manpercons_salary_perhour) + " </td>"
                + "<td>" + object.manpercons_work_time + " Jam </td>"
                + "<td class='pull-right' > Rp. " + formating(object.manpercons_total_cost) + "</td>"
                + "<td>"
                + input
                + "<div class='btn-group pull-right'>"
                + "<button type='button' class='btn btn-edit btn-success btn-xs' data-rowid='" + rowid_ + "'>"
                + "<i class='fa fa-edit'></i>"
                + "</button>"
                + "<button type='button' class='btn btn-remove btn-danger btn-xs'>"
                + "<i class='fa fa-trash'></i>"
                + "</button>"
                + "</div>"
                + "</td>"
                + "</tr>";

        // Check is update or new
        if (rowid == undefined) {

            // Remove empty row
            $('.empty-row').remove();

            // Append row into table
            $('#table-manpercons').find('tbody').append(html);

        } else {

            // Replace with new value
            $('#table-manpercons').find('tr[data-id="' + rowid + '"]').replaceWith(html);

        }


    }

    function manpower_reset_form() {

        // Reset form
        $('#modal-manpower').find('input[name="manpercons_id"]').val('');
        $('#modal-manpower').find('select[name="manpercons_manpower_id"]').val('').change();
        $('#modal-manpower').find('input[name="manpercons_coefficient"]').val('');
        $('#modal-manpower').find('input[name="manpercons_work_time"]').val('');

        // Row variable
        rowid = null;

    }

    function manpower_calculation() {

        // Variable initializaion
        var total_cost_perconst = 0;

        // Foreach data 
        $('input[name="manpower_items[]"]').each(function (index, value) {

            // Get value
            var val_ = $(value).val();

            // Convert json data into object
            val_ = JSON.parse(val_);

            // Total material setup time
            total_cost_perconst += parseFloat(val_['manpercons_total_cost']);

        });

        // Rounded total cost per construction
        total_cost_perconst = Number((total_cost_perconst).toFixed(1));

        // Formating total cost per construction
        total_cost_perconst = formating(total_cost_perconst);

        // Append return
        $('.manpowers-cost').text(total_cost_perconst);


    }

    function manpower_ajax(id) {

        // Get data from server 
        $.get(manpercons_url + '/' + id, function (passed) {

            // Break down object 
            $.each(passed, function (index, data) {

                console.log(data);

                var manpercons_total_cost = manpower_total_cost(data.manpercons_coefficient, data.manpercons_work_time, data.manpercons_salary_perhour);

                // Build object 
                var object = {};
                object.manpercons_id = data.manpercons_id;
                object.manpercons_construction_id = data.manpercons_construction_id;
                object.manpercons_manpower_id = data.manpercons_manpower_id;
                object.manpercons_coefficient = data.manpercons_coefficient;
                object.manpercons_work_time = data.manpercons_work_time;
                object.manpercons_salary_perhour = data.manpercons_salary_perhour;
                object.manpercons_total_cost = Number((manpercons_total_cost).toFixed(1));
                object.manpercons_manpower_detail = data.manpowers;

                // Append data into table
                manpower_html_appending(object);

            });

        }).done(function () {

            // Calculation material items
            manpower_calculation();

        });

    }



</script>