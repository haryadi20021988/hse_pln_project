<script type="text/javascript">

    // Global variable
    var equipercons_url = $('input[name="equipercons_url"]').val();
    var equipment_deleting = [];
    var equipment_rowid = null;
    var equipment_empty_row = '<tr class="empty-row"><td colspan="5" style="text-align: center;">No Data Available</td></tr>';
    var equipment_btn_add = '<button type="button" class="btn btn-add btn-info"><i class="fa fa-plus"></i> Tambah</button>';
    var equipment_btn_edit = '<button type="button" class="btn btn-update btn-success">'
            + '<i class="fa fa-save"></i> Perbarui'
            + '</button>&nbsp;'
            + '<button type="button" class="btn btn-cancel btn-danger">'
            + '<i class="fa fa-times"></i> Batal'
            + '</button>';

    // Open modal action
    $('#modal-equipment').on('shown.bs.modal', function (e) {

        // Activating select2
        $('.select2').select2();

    });

    // Open new modal
    $('body').on('click', '.btn-add-equipment', function () {

        // Variable initialization
        var id = $(this).attr('data-id');

        // Initialization title
        $('#modal-equipment').find('.modal-title-placeholder').text('Tambah Data Perlengkapan');

        // Append construction id
        $('#modal-equipment').find('input[name="equipercons_construction_id"]').val(id);

        // Show modal
        $('#modal-equipment').modal();

        // Synchronize data from serve
        equipment_ajax(id);

    });

    // Close modal action
    $('#modal-equipment').on('hidden.bs.modal', function (e) {

        // Reset all placeholder into empty condition
        $('#modal-equipment').find('.modal-title-placeholder').text('');

        // Remove id construction
        $('#modal-equipment').find('input[name="equipercons_construction_id"]').val('');

        // Clear table
        $('#table-equipercons').find('tbody').html(equipment_empty_row);

    });

    // Add material items
    $('#modal-equipment').on('click', '.btn-add', function () {

        // Form validation
        if (equipment_validation() == false) {
            return false;
        }

        // Variable initialization
        var object = equipment_grab_form();

        // Append data into html
        equipment_html_appending(object);

        // Calculation setup time
        equipment_calculation();

        // Reset form
        equipment_reset_form();

    });


    // Add material items
    $('#modal-equipment').on('click', '.btn-edit', function () {

        // Get data from row table
        var data = $(this).parent().parent().find('input[name="equipment_items[]"]').val();

        // Convert data from string json into javascript object
        data = JSON.parse(data);

        // Get row id
        equipment_rowid = $(this).attr('data-rowid');

        // Append data into form
        $('input[name="equipercons_id"]').val(data['equipercons_id']);
        $('select[name="equipercons_equipment_id"]').val(data['equipercons_equipment_id']).change();
        $('input[name="equipercons_use_time"]').val(data['equipercons_use_time']);

        // Swith button
        $('#modal-equipment').find('.btn-placeholder').html(equipment_btn_edit);


    });

    // Update material action
    $('body').on('click', '.btn-update', function () {

        // Form validation
        if (equipment_validation() == false) {
            return false;
        }

        // Variable initializaion
        var object = equipment_grab_form();

        // Append data into html
        equipment_html_appending(object, equipment_rowid);

        // Calculation setup time
        equipment_calculation();

        // Reset form
        equipment_reset_form();

        // Swith button
        $('#modal-equipment').find('.btn-placeholder').html(equipment_btn_add);

    });

    // Cancel button action
    $('#modal-equipment').on('click', '.btn-cancel', function () {

        // Reset form
        equipment_reset_form();

        // Swith button
        $('#modal-equipment').find('.btn-placeholder').html(equipment_btn_add);

    });

    // Delete row
    $('#modal-equipment').on('click', '.btn-remove', function () {

        // Get dom object
        var dom = $(this).parent().parent().parent();

        // Open delete modal confirmation
        $('#modal-delete-ajax-confirmation').modal({
            backdrop: 'static',
            keyboard: false
        });

        equipment_remove_apply(dom);

    });

    // Remove apply
    function equipment_remove_apply(dom) {

        $('#modal-delete-ajax-confirmation').on('click', '.btn-remove-apply', function (event) {

            // Get data
            var data_ = $(dom).find('input[name="equipment_items[]"]').val();

            // Convert json string into javascript array or object
            data_ = JSON.parse(data_);

            // Append id material into array stack when id is exit
            if (data_['equipercons_id'] !== '') {

                // Add id into array
                equipment_deleting.push(data_['equipercons_id']);

                // Convert array into json string
                var deleting_ = JSON.stringify(equipment_deleting);

                // Append array data into input
                $('#modal-equipment').find('input[name="equipercons_deleted"]').val(deleting_);

            }

            // Remove row
            $(dom).remove();

            // Calculation setup time
            equipment_calculation();

            // Get row length in table
            var row_length = $('#table-equipercons').find('tbody tr').length;

            // Check wheter row is empty or not
            if (row_length == 0) {
                $('#table-equipercons').find('tbody').append(equipment_empty_row);
            }

            // Close delete modal confirmation
            $('#modal-delete-ajax-confirmation').modal('hide');

            // Remove event
            $("#modal-delete-ajax-confirmation").off("click", ".btn-remove-apply");

        });

    }


    // Save materials item action
    $('#modal-equipment').on('click', '.btn-save', function () {

        // Check is row already exist
        var row_length = $('input[name="equipment_items[]"]').length;

        // Submit when row length is filled
        if (row_length > 0) {

            // Submit form
            $('.form-equipment').submit();

        } else {

            // Show error alert
            table_alert('Data equipment masih kosong');

        }

    });

    function equipment_grab_form() {

        // Variable initialization
        var object = {};
        var equipercons_id = $('#modal-equipment').find('input[name="equipercons_id"]').val();
        var equipercons_construction_id = $('#modal-equipment').find('input[name="equipercons_construction_id"]').val();
        var equipercons_equipment_id = $('#modal-equipment').find('select[name="equipercons_equipment_id"] option:selected').val();
        var equipercons_detail = $('#modal-equipment').find('select[name="equipercons_equipment_id"] option:selected').attr('data-value');
        var equipercons_use_time = $('input[name="equipercons_use_time"]').val();
        var equipercons_hourlycost = $('#modal-equipment').find('select[name="equipercons_equipment_id"] option:selected').attr('data-hourlycost');
        var equipercons_totalcost = equipercons_use_time * equipercons_hourlycost;

        // Build data collected
        object.equipercons_id = equipercons_id;
        object.equipercons_construction_id = equipercons_construction_id;
        object.equipercons_equipment_id = equipercons_equipment_id;
        object.equipercons_detail = equipercons_detail;
        object.equipercons_use_time = equipercons_use_time;
        object.equipercons_hourlycost = equipercons_hourlycost;
        object.equipercons_totalcost = equipercons_totalcost;

        // Return data as object
        return object;

    }

    function equipment_validation() {

        // Variable initialization
        var is_valid = true;
        var equipercons_equipment_id = $('#modal-equipment').find('select[name="equipercons_equipment_id"] option:selected').val();
        var equipercons_use_time = $('#modal-equipment').find('input[name="equipercons_use_time"]').val();

        // Clear error validation
        clear_error_message();

        // Check is material has selected
        if (equipercons_equipment_id == '') {

            // Get parent dom
            var dom = $('#modal-equipment').find('select[name="equipercons_equipment_id"]');

            // Show error message
            error_message(dom, 'Please select equipment');

            // Set flag
            is_valid = false;

        }

        // Check is quantity has filled
        if (equipercons_use_time == '') {

            // Get parent dom
            var dom = $('#modal-equipment').find('input[name="equipercons_use_time"]');

            // Show error message
            error_message(dom, 'Please input equipment use time');

            // Set flag
            is_valid = false;

        }

        // Return check result
        return is_valid;

    }

    function equipment_html_appending(object, rowid) {

        // Check is update or new
        if (rowid == undefined) {
            if (object.equipercons_id == '') {
                // Remove empty row
                var rowid_ = guid();
            } else {
                var rowid_ = object.equipercons_id;
            }
        } else {
            // Replace with new value
            var rowid_ = rowid;
        }

        // Variable initalization
        var data = JSON.parse(object.equipercons_detail);
        var input = "<input type='hidden' name='equipment_items[]' value='" + JSON.stringify(object) + "' />";
        var html = "<tr data-id='" + rowid_ + "' >"
                + "<td>" + data['equipment_type'] + "</td>"
                + "<td class='pull-right' > Rp. " + formating(object.equipercons_hourlycost) + " </td>"
                + "<td>" + object.equipercons_use_time + " Jam </td>"
                + "<td class='pull-right' > Rp. " + formating(object.equipercons_totalcost) + " </td>"
                + "<td>"
                + input
                + "<div class='btn-group pull-right'>"
                + "<button type='button' class='btn btn-edit btn-success btn-xs' data-rowid='" + rowid_ + "'>"
                + "<i class='fa fa-edit'></i>"
                + "</button>"
                + "<button type='button' class='btn btn-remove btn-danger btn-xs'>"
                + "<i class='fa fa-trash'></i>"
                + "</button>"
                + "</div>"
                + "</td>"
                + "</tr>";

        // Check is update or new
        if (rowid == undefined) {

            // Remove empty row
            $('.empty-row').remove();

            // Append row into table
            $('#table-equipercons').find('tbody').append(html);

        } else {

            // Replace with new value
            $('#table-equipercons').find('tr[data-id="' + rowid + '"]').replaceWith(html);

        }

    }

    function equipment_reset_form() {

        // Reset form
        $('#modal-equipment').find('input[name="equipercons_id"]').val('');
        $('#modal-equipment').find('select[name="equipercons_equipment_id"]').val('').change();
        $('#modal-equipment').find('input[name="equipercons_use_time"]').val('');

        // Row variable
        rowid = null;

    }

    function equipment_calculation() {

        // Variable initializaion
        var total_equipment_cost = 0;

        // Foreach data
        $('input[name="equipment_items[]"]').each(function (index, value) {

            // Get value
            var val_ = $(value).val();

            // Convert json data into object
            val_ = JSON.parse(val_);

            // Total material setup time
            total_equipment_cost += val_['equipercons_totalcost'];

        });

        // Append return
        $('.total-equip-cost').text(formating(total_equipment_cost.toFixed(2)));

    }

    function equipment_ajax(id) {

        // Get data from server
        $.get(equipercons_url + '/' + id, function (passed) {

            // Break down object
            $.each(passed, function (index, data) {
                
                // Variable initialization
                var equipercons_hourlycost = equipment(data['equipments']['equipment_price'], data['equipments']['equipment_lifetime'], 2);

                // Build object
                var object = {};
                object.equipercons_id = data['equipercons_id'];
                object.equipercons_construction_id = data['equipercons_construction_id'];
                object.equipercons_equipment_id = data['equipercons_equipment_id'];
                object.equipercons_detail = JSON.stringify(data['equipments']);
                object.equipercons_use_time = data['equipercons_use_time'];
                object.equipercons_hourlycost = equipercons_hourlycost;
                object.equipercons_totalcost = data['equipercons_use_time'] * equipercons_hourlycost;

                // Append data into table
                equipment_html_appending(object);


            });

        }).done(function () {

            // Calculation material items
            equipment_calculation();

        });

    }



</script>
