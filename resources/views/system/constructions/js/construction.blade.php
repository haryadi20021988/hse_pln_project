<script type="text/javascript">

    // Global variable initialization
    var base_url = $('input[name="base_url"]').val();
    var method_ = '<input type="hidden" name="_method" value="PUT">';
    var error = $('input[name="error"]').val();
    var btn_add = '<button type="submit" class="btn btn-info btn-add"><i class="fa fa-plus"></i> Tambah Konstruksi</button>';
    var btn_edit = '<button type="submit" class="btn btn-success btn-update"><i class="fa fa-save"></i> Perbarui Konstruksi</button>'
            + '&nbsp;<button type="button" class="btn btn-danger btn-cancel"><i class="fa fa-times"></i> Batal</button>';

    // First initialization 
    $(document).ready(function () {

        // Check is has error
        if (error !== '') {

            // Convert json string format into javascript object 
            var obj = JSON.parse(error);

            // Break down into array list
            $.each(obj, function (index, value) {
                show_alert(index, value);
            });

        }

    });

    // Datatable 
    $("#table-construction").DataTable({
        "ordering": false,
        "paging": false,
        "searching": false,
        "info": false
    });

    // Edit button action
    $('body').on('click', '.btn-edit', function () {

        // Variable initialiation
        var data = $(this).attr('data-value');

        // Convert data from json string into js object
        data = JSON.parse(data);

        // Append data into form
        $('input[name="construction_name"]').val(data['construction_name']);

        // Change button placeholder
        $('.constructions-form').find('.btn-placeholder').empty();
        $('.constructions-form').find('.btn-placeholder').append(btn_edit);

        // Change form action url
        $('.constructions-form').attr('action', base_url + '/' + data['construction_id']);

        // Append input method_ 
        $('.constructions-form').find('.hidden-input-placehoder').append(method_);

    });

    // Cancel button action
    $('body').on('click', '.btn-cancel', function () {

        // Clear all input form
        $('input[name="construction_name"]').val('');

        // Clear action form url
        $('.constructions-form').attr('action', base_url);

        // Remove methode hidden input 
        $('.constructions-form').find('.hidden-input-placehoder').find('input[name="_method"]').remove();

        // Change button submit
        $('.constructions-form').find('.btn-placeholder').empty();
        $('.constructions-form').find('.btn-placeholder').append(btn_add);

    });

    // Submit
    $('.constructions-form').on('click', '.btn-update', function () {
        $('.constructions-form').submit();
    });

    // Open delete modal confirmation
    show_modal_delete(base_url);

    

  

</script>