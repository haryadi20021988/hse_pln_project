<script type="text/javascript">

    // Global variable
    var matpercons_url = $('input[name="matpercons_url"]').val();
    var material_deleting = [];
    var material_rowid = null;
    var material_empty_row = '<tr class="empty-row"><td colspan="5" style="text-align: center;">No Data Available</td></tr>';
    var material_btn_add = '<button type="button" class="btn btn-add btn-info"><i class="fa fa-plus"></i> Tambah</button>';
    var material_btn_edit = '<button type="button" class="btn btn-update btn-success">'
            + '<i class="fa fa-save"></i> Perbarui'
            + '</button>&nbsp;'
            + '<button type="button" class="btn btn-cancel btn-danger">'
            + '<i class="fa fa-times"></i> Batal'
            + '</button>';

    // Open modal action
    $('#modal-material').on('shown.bs.modal', function (e) {

        // Activating select2
        $('.select2').select2();

    });

    // Open new modal
    $('body').on('click', '.btn-add-material', function () {

        // Variable initialization
        var id = $(this).attr('data-id');

        // Initialization title
        $('#modal-material').find('.modal-title-placeholder').text('Tambah Data Material');

        // Append construction id
        $('#modal-material').find('input[name="matpercons_construction_id"]').val(id);

        // Show modal
        $('#modal-material').modal();

        // Synchronize data from serve
        material_ajax(id);

    });

    // Close modal action
    $('#modal-material').on('hidden.bs.modal', function (e) {

        // Reset all placeholder into empty condition
        $('#modal-material').find('.modal-title-placeholder').text('');

        // Remove id construction
        $('#modal-material').find('input[name="matpercons_construction_id"]').val('');

        // Clear table
        $('#table-matpercons').find('tbody').html(material_empty_row);

    });

    // Add material items
    $('#modal-material').on('click', '.btn-add', function () {

        // Form validation
        if (material_validation() == false) {
            return false;
        }

        // Variable initialization
        var object = material_grab_form();

        // Append data into html
        material_html_appending(object);

        // Calculation setup time
        material_calculation();

        // Reset form
        material_reset_form();

    });

    // Add material items
    $('#modal-material').on('click', '.btn-edit', function () {

        // Get data from row table
        var data = $(this).parent().parent().find('input[name="material_items[]"]').val();

        // Convert data from string json into javascript object
        data = JSON.parse(data);

        // Get row id 
        material_rowid = $(this).attr('data-rowid');

        // Append data into form
        $('input[name="matpercons_id"]').val(data['matpercons_id']);
        $('select[name="matpercons_material_id"]').val(data['matpercons_material_id']).change();
        $('input[name="matpercons_quantity"]').val(data['matpercons_quantity']);

        // Swith button
        $('#modal-material').find('.btn-placeholder').html(material_btn_edit);

    });

    // Update material action
    $('body').on('click', '.btn-update', function () {

        // Form validation
        if (material_validation() == false) {
            return false;
        }

        // Variable initializaion
        var object = material_grab_form();

        // Append data into html
        material_html_appending(object, material_rowid);

        // Calculation setup time
        material_calculation();

        // Reset form
        material_reset_form();

        // Swith button
        $('#modal-material').find('.btn-placeholder').html(material_btn_add);

    });

    // Cancel button action
    $('#modal-material').on('click', '.btn-cancel', function () {

        // Reset form
        material_reset_form();

        // Swith button
        $('#modal-material').find('.btn-placeholder').html(material_btn_add);

    });

    // Delete row
    $('#modal-material').on('click', '.btn-remove', function () {

        // Get dom object
        var dom = $(this).parent().parent().parent();

        // Open delete modal confirmation
        $('#modal-delete-ajax-confirmation').modal({
            backdrop: 'static',
            keyboard: false
        });

        material_remove_apply(dom);

    });

    // Remove apply
    function material_remove_apply(dom) {

        $('#modal-delete-ajax-confirmation').on('click', '.btn-remove-apply', function (event) {

            // Get data 
            var data_ = $(dom).find('input[name="material_items[]"]').val();

            // Convert json string into javascript array or object
            data_ = JSON.parse(data_);

            // Append id material into array stack when id is exit
            if (data_['matpercons_id'] !== '') {

                // Add id into array
                material_deleting.push(data_['matpercons_id']);

                // Convert array into json string
                var deleting_ = JSON.stringify(material_deleting);
                
                // Append array data into input
                $('#modal-material').find('input[name="matpercons_deleted"]').val(deleting_);

            }

            // Remove row
            $(dom).remove();

            // Calculation setup time
            material_calculation();

            // Get row length in table
            var row_length = $('#table-matpercons').find('tbody tr').length;

            // Check wheter row is empty or not
            if (row_length == 0) {
                $('#table-matpercons').find('tbody').append(material_empty_row);
            }

            // Close delete modal confirmation
            $('#modal-delete-ajax-confirmation').modal('hide');

            // Remove event 
            $("#modal-delete-ajax-confirmation").off("click", ".btn-remove-apply");

        });

    }


    // Save materials item action
    $('#modal-material').on('click', '.btn-save', function () {

        // Check is row already exist 
        var row_length = $('input[name="material_items[]"]').length;

        // Submit when row length is filled
        if (row_length > 0) {

            // Submit form
            $('.form-material').submit();

        } else {

            // Show error alert
            table_alert('Data material masih kosong');

        }

    });

    function material_grab_form() {

        // Variable initialization
        var object = {};
        var matpercons_id = $('#modal-material').find('input[name="matpercons_id"]').val();
        var matpercons_material_id = $('#modal-material').find('select[name="matpercons_material_id"] option:selected').val();
        var matpercons_quantity = $('#modal-material').find('input[name="matpercons_quantity"]').val();
        var matpercons_setup_time = $('#modal-material').find('select[name="matpercons_material_id"] option:selected').attr('data-setuptime');
        var matpercons_material_name = $('#modal-material').find('select[name="matpercons_material_id"] option:selected').text();

        // Build data collected
        object.matpercons_id = matpercons_id;
        object.matpercons_material_id = matpercons_material_id;
        object.matpercons_quantity = matpercons_quantity;
        object.matpercons_setup_time = matpercons_setup_time;
        object.matpercons_material_name = matpercons_material_name;

        // Return data as object
        return object;

    }

    function material_validation() {

        // Variable initialization
        var is_valid = true;
        var matpercons_material_id = $('#modal-material').find('select[name="matpercons_material_id"] option:selected').val();
        var matpercons_quantity = $('#modal-material').find('input[name="matpercons_quantity"]').val();

        // Clear error validation
        clear_error_message();

        // Check is material has selected
        if (matpercons_material_id == '') {

            // Get parent dom
            var dom = $('#modal-material').find('select[name="matpercons_material_id"]');

            // Show error message
            error_message(dom, 'Please select material');

            // Set flag
            is_valid = false;

        }

        // Check is quantity has filled
        if (matpercons_quantity == '') {

            // Get parent dom
            var dom = $('#modal-material').find('input[name="matpercons_quantity"]');

            // Show error message
            error_message(dom, 'Please input material quantity');

            // Set flag
            is_valid = false;

        }

        // Return check result
        return is_valid;

    }

    function material_html_appending(object, rowid) {

        // Check is update or new
        if (rowid == undefined) {
            if (object.matpercons_id == '') {
                // Remove empty row
                var rowid_ = guid();
            } else {
                var rowid_ = object.matpercons_id;
            }
        } else {
            // Replace with new value
            var rowid_ = rowid;
        }

        // Variable initalization
        var total_setup_time = parseFloat(object.matpercons_quantity) * parseFloat(object.matpercons_setup_time);
        var input = "<input type='hidden' name='material_items[]' value='" + JSON.stringify(object) + "' />";
        var html = "<tr data-id='" + rowid_ + "' >"
                + "<td>" + object.matpercons_material_name + "</td>"
                + "<td>" + object.matpercons_setup_time + " Menit </td>"
                + "<td>" + object.matpercons_quantity + " </td>"
                + "<td>" + Number((total_setup_time).toFixed(1)) + " Menit</td>"
                + "<td>"
                + input
                + "<div class='btn-group pull-right'>"
                + "<button type='button' class='btn btn-edit btn-success btn-xs' data-rowid='" + rowid_ + "'>"
                + "<i class='fa fa-edit'></i>"
                + "</button>"
                + "<button type='button' class='btn btn-remove btn-danger btn-xs'>"
                + "<i class='fa fa-trash'></i>"
                + "</button>"
                + "</div>"
                + "</td>"
                + "</tr>";

        // Check is update or new
        if (rowid == undefined) {

            // Remove empty row
            $('.empty-row').remove();

            // Append row into table
            $('#table-matpercons').find('tbody').append(html);

        } else {

            // Replace with new value
            $('#table-matpercons').find('tr[data-id="' + rowid + '"]').replaceWith(html);

        }

    }

    function material_reset_form() {

        // Reset form
        $('#modal-material').find('input[name="matpercons_id"]').val('');
        $('#modal-material').find('select[name="matpercons_material_id"]').val('').change();
        $('#modal-material').find('input[name="matpercons_quantity"]').val('');

        // Row variable
        rowid = null;

    }

    function material_calculation() {

        // Variable initializaion
        var total_setup_time = 0;

        // Foreach data 
        $('input[name="material_items[]"]').each(function (index, value) {

            // Get value
            var val_ = $(value).val();

            // Convert json data into object
            val_ = JSON.parse(val_);

            // Total material setup time
            total_setup_time += (parseFloat(val_['matpercons_setup_time']) * parseFloat(val_['matpercons_quantity']));

        });

        // Append return
        $('.setup-time').text(total_setup_time);

    }

    function material_ajax(id) {

        // Get data from server 
        $.get(matpercons_url + '/' + id, function (passed) {

            // Break down object 
            $.each(passed, function (index, data) {

                // Build object 
                var object = {};
                object.matpercons_id = data.matpercons_id;
                object.matpercons_material_id = data.matpercons_material_id;
                object.matpercons_quantity = data.matpercons_quantity;
                object.matpercons_setup_time = data.materials.material_setup_time;
                object.matpercons_material_name = data.materials.material_name;

                // Append data into table
                material_html_appending(object);

            });

        }).done(function () {

            // Calculation material items
            material_calculation();

        });

    }



</script>