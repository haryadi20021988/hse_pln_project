<div class="modal fade"  role="dialog" id="modal-material">
    <div class="modal-dialog modal-width" role="document">
        <div class="modal-content">
            <form class="form-material" action="<?= url('matpercons'); ?>" method="post">

                <!-- List hidden input -->
                <div class="hidden-input-placehoder">
                    {!! csrf_field() !!}
                    <input type="hidden" name="matpercons_construction_id" />
                    <input type="hidden" name="matpercons_deleted" />
                    <input type="hidden" name="matpercons_id" />
                </div>

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <i class="fa fa-cubes"></i>
                        <span class="modal-title-placeholder" ></span>
                    </h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Material</label>
                                <select name="matpercons_material_id" class="form-control select2" >
                                    <option value="">-- Pilih Material --</option>
                                    @foreach($materials as $item)
                                    <option value="{{$item->material_id}}" data-setuptime="{{$item->material_setup_time}}" >
                                        {{$item->material_name}}
                                    </option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jumlah Material</label>
                                <input type="number" step="1" min="0" name="matpercons_quantity" class="form-control"  />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="btn-placeholder btn-top-padding">
                                <button type="button" class="btn btn-add btn-info">
                                    <i class="fa fa-plus"></i> Tambah
                                </button>
                            </div>
                        </div>
                    </div>

                    <hr/>

                    <div class="row">

                        <div class="col-md-12 table-alert">

                        </div>

                        <div class="col-md-12">
                            <table id="table-matpercons" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Material</th>
                                        <th>Waktu *</th>
                                        <th>Jumlah Material</th>
                                        <th>Total Waktu *</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="empty-row">
                                        <td colspan="5" style="text-align: center;">No Data Available</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3">
                                            Total Waktu Pemasangan Material / Konstruksi
                                        </th>
                                        <th>
                                            <span class="setup-time"></span>
                                            Menit
                                        </th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="span-note">
                                * Waktu yang dibutuhkan untuk pemasangan satu material dalam menit
                            </span>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-close btn-default" data-dismiss="modal">
                                <i class="fa fa-times"></i>
                                Tutup
                            </button>
                            <button type="button" class="btn btn-save btn-info">
                                <i class="fa fa-edit"></i>
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->