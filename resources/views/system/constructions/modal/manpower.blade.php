<div class="modal fade" role="dialog" id="modal-manpower">
    <div class="modal-dialog modal-width" role="document">
        <div class="modal-content">
            
            <form class="form-manpower" action="<?= url('manpercons'); ?>" method="post">

                <!-- List hidden input -->
                <div class="hidden-input-placehoder">
                    {!! csrf_field() !!}
                    <input type="hidden" name="manpercons_construction_id" />
                    <input type="hidden" name="manpercons_deleted" />
                    <input type="hidden" name="manpercons_id" />
                </div>

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <i class="fa fa-user-plus"></i>
                        <span class="modal-title-placeholder" ></span>
                    </h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tenaga Kerja</label>
                                <select name="manpercons_manpower_id" class="form-control select2" required="true" >
                                    <option value="">-- Pilih Tenaga Kerja --</option>
                                    @foreach($manpowers as $item)
                                    <option value="{{$item->manpower_id}}" 
                                            data-item="{{ json_encode($item) }}"
                                            data-hourcost="{{ $manpowercalc->set($item->manpower_coeffisien)->hour(1) }}"
                                            >
                                        {{$item->manpower_qualification}}
                                    </option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Koefisien *</label>
                                <input type="number" step="0.1" min="0" max="10" name="manpercons_coefficient" class="form-control" required="true" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Durasi Kerja (Jam)</label>
                                <input type="number" step="0.1"  min="0" max="100" name="manpercons_work_time" class="form-control" required="true" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="btn-placeholder btn-top-padding">
                                <button type="button" class="btn btn-add btn-info">
                                    <i class="fa fa-plus"></i> Tambah
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="span-note">
                                * Koefisien waktu pekerjaan
                            </span>
                        </div>
                    </div>

                    <hr/>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="col-md-12 table-alert">

                            </div>

                            <table id="table-manpercons" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Tenaga Kerja</th>
                                        <th>Koefisien</th>
                                        <th>Biaya / Jam</th>
                                        <th>Durasi Kerja</th>
                                        <th>Total Biaya</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="empty-row">
                                        <td colspan="6" style="text-align: center;">No Data Available</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4">
                                            Total Biaya Tenaga Kerja / Konstruksi
                                        </th>
                                        <th class="pull-right" >
                                            Rp. <span class="manpowers-cost"></span>
                                        </th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-close btn-default" data-dismiss="modal">
                                <i class="fa fa-times"></i>
                                Tutup
                            </button>
                            <button type="button" class="btn btn-save btn-info">
                                <i class="fa fa-edit"></i>
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->