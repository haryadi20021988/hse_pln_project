<div class="modal fade" role="dialog" id="modal-equipment">
    <div class="modal-dialog modal-width" role="document">
        <div class="modal-content">
            <form class="form-equipment" action="<?= url('equipercons'); ?>" method="post">

                <!-- List hidden input -->
                <div class="hidden-input-placehoder">
                    {!! csrf_field() !!}
                    <input type="hidden" name="equipercons_construction_id" />
                    <input type="hidden" name="equipercons_deleted" />
                    <input type="hidden" name="equipercons_id" />
                </div>

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <i class="fa fa-wrench"></i>
                        <span class="modal-title-placeholder" ></span>
                    </h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Peralatan Kerja</label>
                                <select name="equipercons_equipment_id" class="form-control select2">
                                    <option value="">-- Pilih Perlengkapan --</option>
                                    @foreach($equipments as $item)
                                    <option 
                                        value="{{$item->equipment_id}}" 
                                        data-value="{{json_encode($item)}}" 
                                        data-hourlycost="<?= $equipmentcalc->init($item->equipment_price, $item->equipment_lifetime)->get(2); ?>"
                                        >
                                        {{$item->equipment_type}}
                                    </option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Waktu Pemakaian (Jam)</label>
                                <input type="number" step="0.1" name="equipercons_use_time" class="form-control" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="btn-placeholder btn-top-padding">
                                <button type="button" class="btn btn-add btn-info">
                                    <i class="fa fa-plus"></i> Tambah
                                </button>
                            </div>
                        </div>
                    </div>

                    <hr/>

                    <div class="row">

                        <div class="col-md-12 table-alert">
                        </div>

                        <div class="col-md-12">
                            <table id="table-equipercons" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Peralatan</th>
                                        <th>Biaya / Jam</th>
                                        <th>Waktu Pemakaian ( Jam )</th>
                                        <th>Total Biaya</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="empty-row">
                                        <td colspan="5" style="text-align: center;">No Data Available</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3">
                                            Total Biaya Peralatan / Konstruksi
                                        </th>
                                        <th class="pull-right">
                                            Rp. <span class="total-equip-cost"></span>
                                        </th>
                                        <th>

                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-close btn-default" data-dismiss="modal">
                                <i class="fa fa-times"></i>
                                Tutup
                            </button>
                            <button type="button" class="btn btn-save  btn-info">
                                <i class="fa fa-edit"></i>
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->