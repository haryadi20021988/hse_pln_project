<div class="modal fade" tabindex="-1" role="dialog" id="modal-detail">
    <div class="modal-dialog modal-width" role="document">
        <div class="modal-content">
            <form class="form-detail" action="" method="post">

                <!-- List hidden input -->
                {!! csrf_field() !!}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <i class="fa fa-file-text-o"></i>
                        <span class="modal-title-placeholder" ></span>
                    </h4>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <tbody class="">
                                    <tr>
                                        <td width="30%" >
                                            <label> Jenis Konstruksi </label><br/>
                                            <span id="detail_construction_name">-</span>
                                        </td>
                                        <td>
                                            <label> Total Biaya</label><br/>
                                            <span id="detail_construction_total_cost" >Rp. <span>-</span>,- </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default flat flash">
                                    <div class="panel-heading" role="tab" id="headingTri">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTri" aria-expanded="false" aria-controls="collapseTri">
                                                <i class="fa fa-user-plus"></i> Tenaga Kerja
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTri" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTri">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-striped table-hover" id="detail-manpower-table">
                                                        <thead>
                                                            <tr>
                                                                <th>Tenaga Kerja</th>
                                                                <th>Koefisien</th>
                                                                <th>Biaya / Jam</th>
                                                                <th>Durasi Kerja</th>
                                                                <th>Total Biaya</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody >
                                                            <tr class="empty-row"><td colspan="5" style="text-align: center;">No Data Available</td></tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th colspan="4">
                                                                    Total Biaya Tenaga Kerja / Konstruksi
                                                                </th>
                                                                <th class="pull-right">
                                                                    Rp. <span id="detail-manpower-total-cost">-</span>
                                                                </th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <i class="fa fa-wrench"></i> Peralatan
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-striped table-hover" id="detail-equipments-table">
                                                        <thead>
                                                            <tr>
                                                                <th>Perlengkapan</th>
                                                                <th>Biaya / Jam</th>
                                                                <th>Waktu Pemakaian ( Jam )</th>
                                                                <th>Total Biaya</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="empty-row"><td colspan="4" style="text-align: center;">No Data Available</td></tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th colspan="3">
                                                                    Total Biaya Peralatan / Konstruksi
                                                                </th>
                                                                <th class="pull-right">
                                                                    Rp. <span id="detail-equipment-total-cost">-</span>
                                                                </th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-heading mobile-discount-input" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-cubes"></i> Material
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-striped table-hover dmt" id="detail-meterial-table">
                                                        <thead>
                                                            <tr>
                                                                <th>Material</th>
                                                                <th>Waktu *</th>
                                                                <th>Jumlah Material</th>
                                                                <th>Total Waktu *</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="empty-row"><td colspan="4" style="text-align: center;">No Data Available</td></tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th colspan="3">
                                                                    Total Waktu Pemasangan Material / Konstruksi
                                                                </th>
                                                                <th>
                                                                    <span id="detail-meterial-total-cost">-</span> Jam
                                                                </th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <i class="fa fa-times"></i>
                                Tutup
                            </button>
                            <a href="<?= url('download-detail-report') ?>" class="btn btn-default btn-detail-download">
                                <i class="fa fa-print"></i>
                                Cetak
                            </a>
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-edit"></i>
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->