@extends('system.masters.layout')

@section('css')
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= url('adminlte/plugins/select2/select2.min.css') ?>">
@stop

@section('hidden_input')
<input type="hidden" name="base_url" value="<?= url('constructions'); ?>" />
<input type="hidden" name="matpercons_url" value="<?= url('matpercons'); ?>" />
<input type="hidden" name="manpercons_url" value="<?= url('manpercons'); ?>" />
<input type="hidden" name="equipercons_url" value="<?= url('equipercons'); ?>" />
<input type="hidden" name="interest_rate" value="<?= config('setting.interest_rate'); ?>" />
<input type="hidden" name="detail_constrcution_url" value="<?= url('download-detail-report'); ?>" />
@stop

@section('modal')
@include('system.constructions.modal.equipment')
@include('system.constructions.modal.material')
@include('system.constructions.modal.manpower')
@include('system.constructions.modal.detail')
@include('system.masters.modal.delete-ajax')
@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

        @include('system.masters.alert.error')
        @include('system.masters.alert.success')

        <div class="box box-solid administrator">
            <div class="box-header with-border">
                <h3 class="box-title">Form Input Data Konstruksi</h3>
            </div>
            <div class="box-body">

                <form class="constructions-form" method="post" >

                    <div class="hidden-input-placehoder">
                        {!! csrf_field() !!}    
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jenis Konstruksi</label>
                                <input value="<?= old('construction_name'); ?>" type="text" name="construction_name" class="form-control" required="true" />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="btn-placeholder btn-top-padding">
                                    <button type="submit" class="btn btn-info btn-add">
                                        <i class="fa fa-plus"></i> Tambah Data Konstruksi
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header with-border box-header-padding">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="box-title">Daftar Konstruksi</h3>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input name="input_search" 
                                           type="text" 
                                           class="form-control" 
                                           placeholder="Masukkan kata kunci.. ." 
                                           aria-label="Search for..."
                                           value="<?= $input_search ?>"
                                           >
                                    <span class="input-group-btn">
                                        <button class="btn btn-search btn-default" type="button" >
                                            <i class="fa fa-search"></i> Cari Data
                                        </button>
                                        <a class="btn btn-default" href="<?= url('constructions') ?>" >
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table-construction" class="table table-borderless table-striped table-hover table-font-modif">
                                    <thead>
                                        <tr>
                                            <th width="29%" >
                                                Jenis Konstruksi
                                            </th>
                                            <th width="16%" >
                                                Tenaga Kerja <br> per Jam
                                            </th>
                                            <th width="16%" >
                                                Peralatan <br> per Jam
                                            </th>
                                            <th width="14%" >
                                                WPM* 
                                            </th>
                                            <th width="13%" >
                                                Total Biaya <br> per Konstruksi
                                            </th>
                                            <th width="12%" ></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->construction_name }}</td>
                                            <td>
                                                <button class="btn btn-default btn-add-manpower btn-xs administrator" data-toggle="tooltip" data-placement="top" title="Manipulasi Data Tenaga Kerja" data-id="{{$item->construction_id}}" >
                                                    <i class="fa fa-user-plus"></i>
                                                </button> &nbsp;
                                                Rp.{{ number_format($item->manpower_cost, 2 , ',' , '.') }}
                                            </td>
                                            <td>
                                                <button class="btn btn-default btn-add-equipment btn-xs administrator" data-toggle="tooltip" data-placement="top" title="Manipulasi Data Peralatan" data-id="{{$item->construction_id}}" >
                                                    <i class="fa fa-wrench"></i>
                                                </button> &nbsp;
                                                Rp.{{ number_format($item->equipment_cost , 2 , ',' , '.') }}
                                            </td>
                                            <td>
                                                <button class="btn btn-default btn-add-material btn-xs administrator" data-toggle="tooltip" data-placement="top" title="Manipulasi Data Material" data-id="{{$item->construction_id}}" >
                                                    <i class="fa fa-cubes"></i>
                                                </button> &nbsp;
                                                {{ ($item->wpm)?$item->wpm:'-' }}
                                            </td>
                                            <td>Rp.{{ number_format($item->total_cost, 2 , ',' , '.') }}</td>
                                            <td>
                                                <div class="pull-right">

                                                    <div class="btn-group">
                                                        <button class="btn btn-default btn-add-detail btn-xs" data-toggle="tooltip" data-placement="top" title="Lihat Detail Konstruksi" data-id="{{$item->construction_id}}" >
                                                            <i class="fa fa-search-plus"></i>
                                                        </button>
                                                    </div>

                                                    <div class="btn-group administrator">
                                                        <button 
                                                            class="btn btn-edit btn-success btn-xs" 
                                                            data-toggle="tooltip" 
                                                            data-placement="top" 
                                                            title="Ubah Data Konstruksi" 
                                                            data-value='<?= json_encode($item); ?>'
                                                            >
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button 
                                                            class="btn btn-delete btn-danger btn-xs" 
                                                            data-toggle="tooltip" 
                                                            data-placement="top" 
                                                            title="Hapus Data Material" 
                                                            data-id='<?= $item->construction_id; ?>'
                                                            >
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="page-link pull-right">
                                    {{ $data->appends(['search' => $input_search ])->links() }}
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer with-border">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="span-note">
                                    *WPM (Waktu Pemasangan Material per Konstruksi dalam menit)
                                </span>
                            </div>
                            <div class="col-md-6">
                                <div class="btn-group pull-right">
                                    <a href="<?= url('download-all-report') ?>" class="btn btn-default">
                                        <i class="fa fa-print"></i> Cetak Laporan
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@stop

@section('js')

<!-- Select2 -->
<script src="<?= url('adminlte/plugins/select2/select2.full.min.js'); ?>"></script>
<script src="<?= url('adminlte/plugins/utility/guid.js'); ?>"></script>
<script type="text/javascript">

// Activating select2
$('select2').select2();

</script>

@include('system.constructions.js.construction')
@include('system.constructions.js.equipment')
@include('system.constructions.js.material')
@include('system.constructions.js.manpower')
@include('system.constructions.js.detail')

@stop