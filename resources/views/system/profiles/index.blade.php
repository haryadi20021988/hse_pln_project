@extends('system.masters.layout')

@section('hidden_input')
<input type="hidden" name="base_url" value="<?= url('equipments') ?>" />
@stop

@section('modal')

@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

        @include('system.masters.alert.error')
        @include('system.masters.alert.success')

        <div class="row">
            <div class="col-md-12">

                <div class="box">

                    <!-- /.box-header -->
                    <div class="box-header with-border box-header-padding">
                        <h3 class="box-title">Profil Anda</h3>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="<?= url('profile/' . session('passport')['user_id']); ?>" method="post" >

                                    <div class="hidden-input-placehoder">
                                        {!! csrf_field() !!}    
                                        {{ method_field('PUT') }}
                                    </div>

                                    <table class="table table-striped" style="font-size: 16px;">
                                        <tbody>
                                            <tr>
                                                <td width='20%' >
                                                    Nama Lengkap
                                                </td>
                                                <td width='50%' ></td>
                                                <td width='30%' >
                                                    <input type="text" name="user_fullname" class="form-control" required="true" value="{{ $data['user_fullname'] }}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Nomor Induk Pegawai
                                                </td>
                                                <td></td>
                                                <td>
                                                    <input type="text" name="user_nip" class="form-control" required="true" value="{{ $data['user_nip'] }}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Email
                                                </td>
                                                <td></td>
                                                <td>
                                                    <input type="email" name="user_email" class="form-control" required="true" value="{{ $data['user_email'] }}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width='20%' >
                                                    Nama Pengguna
                                                </td>
                                                <td width='50%' ></td>
                                                <td width='30%' >
                                                    <input type="text" name="user_username" class="form-control" required="true" value="{{ $data['user_username'] }}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Password Baru
                                                </td>
                                                <td></td>
                                                <td>
                                                    <input type="password" name="user_password" class="form-control" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <div class="btn-group pull-right">
                                                        <button class="btn btn-info" type="submit" >
                                                            <i class="fa fa-save"></i> &nbsp; Update
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer with-border">
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@stop

@section('js')
@include('system.equipments.js.index')
@stop