@extends('system.masters.layout')

@section('hidden_input')
<input type="hidden" name="base_url" value="<?= url('users') ?>" />
@stop

@section('modal')
@include('system.users.modal.user')
@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

        @include('system.masters.alert.error')
        @include('system.masters.alert.success')

        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header with-border box-header-padding">
                        <h3 class="box-title">Daftar Pengguna</h3>
                        <div class="pull-right box-tools">
                            <div class="btn-group">
                                <button class="btn btn-info btn-add-user">
                                    <i class="fa fa-plus"></i> Tambah Pengguna
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input name="input_search" 
                                           type="text" 
                                           class="form-control" 
                                           placeholder="Masukkan kata kunci.. ." 
                                           aria-label="Search for..."
                                           value="<?= $input_search ?>"
                                           >
                                    <span class="input-group-btn">
                                        <button class="btn btn-search btn-default" type="button" >
                                            <i class="fa fa-search"></i> Cari Data
                                        </button>
                                        <a class="btn btn-default" href="<?= url('users') ?>" >
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table-users" class="table table-striped table-hover table-font-modif">
                                    <thead>
                                        <tr>
                                            <th width="22%" >
                                                Nama
                                            </th>
                                            <th width="10%" >
                                                NIP
                                            </th>
                                            <th width="20%" >
                                                Email
                                            </th>
                                            <th width="15%" >
                                                Username
                                            </th>
                                            <th width="15%" >
                                                Jenis Pengguna
                                            </th>
                                            <th width="10%" >
                                                Status
                                            </th>
                                            <th width="8%" ></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->user_fullname }}</td>
                                            <td>{{ $item->user_nip }}</td>
                                            <td>{{ $item->user_email }}</td>
                                            <td>{{ $item->user_username }}</td>
                                            <td>
                                                <label class="label <?= ($item->user_type == 'Administrator' ) ? 'label-info' : 'label-default' ?>">
                                                    <i class="fa <?= ($item->user_type == 'Administrator' ) ? 'fa-user-secret' : 'fa fa-user' ?>"></i> {{ $item->user_type }}
                                                </label>
                                            </td>
                                            <td>
                                                @if($item->user_status == 1)
                                                <label class="label label-success">
                                                    <i class="fa fa-check"></i> Aktif
                                                </label>
                                                @else
                                                <label class="label label-danger">
                                                    <i class="fa fa-times"></i> Nonaktif
                                                </label>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group pull-right">
                                                    <button 
                                                        class="btn btn-success btn-edit btn-xs" 
                                                        title="Ubah Data Konstruksi" 
                                                        data-toggle="tooltip" 
                                                        data-placement="top" 
                                                        data-id="{{ $item->user_id }}"
                                                        data-value='<?= json_encode($item); ?>'
                                                        >
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button 
                                                        class="btn btn-danger btn-delete btn-xs" 
                                                        title="Hapus Data Material" 
                                                        data-toggle="tooltip" 
                                                        data-placement="top" 
                                                        data-id="{{ $item->user_id }}"
                                                        >
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="page-link pull-right">
                                    {{ $data->appends(['search' => $input_search ])->links() }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer with-border">

                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@stop

@section('js')
@include('system.users.js.user')
@stop