<div class="modal fade" tabindex="-1" role="dialog" id="modal-user">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-user" action="" method="">

                <!-- List hidden input -->
                <div class="hidden-input-placehoder">
                    {!! csrf_field() !!}    
                </div>

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <i class="fa fa-user"></i>
                        <span class="modal-title-placeholder" ></span>
                    </h4>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            @include('system.masters.alert.error')    
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Pengguna</label>
                                <input type="text" class="form-control" name="user_fullname" value="{{ old('user_fullname') }}" />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nomor Induk Pegawai</label>
                                <input type="text" class="form-control" name="user_nip" value="{{ old('user_nip') }}" />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="user_email" value="{{ old('user_email') }}"  />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="user_username" value="{{ old('user_username') }}" />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="user_password" value="{{ old('user_password') }}" />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tipe Pengguna</label>
                                <div class="checkbox">
                                    <label style="padding-left: 0px;">
                                        <input 
                                            type="radio" 
                                            name="user_type" 
                                            value="Administrator"  
                                            class="radio-default"
                                            <?= (old('user_type') == 'Administrator' || !old('user_type')) ? 'checked="true" ' : '' ?>
                                            />
                                        &nbsp; Administrator
                                    </label>
                                    <label>
                                        <input 
                                            type="radio" 
                                            name="user_type" 
                                            value="Staff" 
                                            <?= (old('user_type') == 'Staff') ? 'checked="true" ' : '' ?>
                                            />
                                        &nbsp; Staff
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status</label>
                                <div class="checkbox">
                                    <label style="padding-left: 0px;">
                                        <input 
                                            type="radio" 
                                            name="user_status" 
                                            value="1" 
                                            checked="true" 
                                            class="radio-default"
                                            <?= (old('user_status') == '1' || !old('user_status')) ? 'checked="true" ' : '' ?>
                                            />
                                        &nbsp; Aktif
                                    </label>
                                    <label>
                                        <input 
                                            type="radio" 
                                            name="user_status" 
                                            value="0" 
                                            <?= (old('user_status') == '0') ? 'checked="true" ' : '' ?>
                                            />
                                        &nbsp; Tidak Aktif
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <i class="fa fa-times"></i>
                                Tutup
                            </button>
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-edit"></i>
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->