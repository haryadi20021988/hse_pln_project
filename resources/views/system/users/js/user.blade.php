<script type="text/javascript">

    // Global variable initialization
    var base_url = $('input[name="base_url"]').val();
    var error = $('input[name="error"]').val();

    // First initialization 
    $(document).ready(function () {

        // Check is has error
        if (error !== '') {

            // Convert json string format into javascript object 
            var obj = JSON.parse(error);

            // Break down into array list
            $.each(obj, function (index, value) {
                show_alert(index, value);
            });

            // Open modal
            open_create();

        }

    });

    // Datatable 
    $("#table-users").DataTable({
        "ordering": false,
        "paging": false,
        "searching": false,
        "info": false
    });

    // Open new modal
    $('body').on('click', '.btn-add-user', function () {
        open_create();
    });

    // Open edit modal
    $('body').on('click', '.btn-edit', function () {

        // Get id data 
        var id = $(this).attr('data-id');
        var data = $(this).attr('data-value');

        // Open update modal
        open_update(id);

        // Populating data
        populate_data(data);

    });

    // Create form on modal
    function open_create() {

        // Initialization title
        $('#modal-user').find('.modal-title-placeholder').text('Tambah Data Pengguna');

        // Form action intialization
        $('#modal-user').find('form').attr('action', base_url);

        // Set form method 
        $('#modal-user').find('form').attr('method', "post");

        // Remove method_ input
        $('input[name="_method"]').remove();

        // Show modal
        $('#modal-user').modal({
            backdrop: 'static',
            keyboard: false
        });

    }

    // Create form on modal
    function open_update(id) {

        // Variable initialization
        var method_ = '<input type="hidden" name="_method" value="PUT">';

        // Initialization title
        $('#modal-user').find('.modal-title-placeholder').text('Perbarui Data Pengguna');

        // Form action intialization
        $('#modal-user').find('form').attr('action', base_url + '/' + id);

        // Set form method 
        $('#modal-user').find('form').attr('method', "post");

        // Append input put method
        $('.hidden-input-placehoder').append(method_);

        // Show modal
        $('#modal-user').modal({
            backdrop: 'static',
            keyboard: false
        });

    }

    // Open delete modal confirmation
    show_modal_delete(base_url);

    // Close modal action
    $('#modal-user').on('hidden.bs.modal', function (e) {

        // Reset all placeholder into empty condition
        $('#modal-user').find('.modal-title-placeholder').text('');

        // Reset all form group
        reset_form_group();

        // Clean input 
        clean_input_value();

    });

</script>