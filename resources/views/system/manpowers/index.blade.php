@extends('system.masters.layout')

@section('hidden_input')
<input type="hidden" name="base_url" value="<?= url('manpowers') ?>" />
@stop

@section('modal')

@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

        @include('system.masters.alert.error')
        @include('system.masters.alert.success')

        <div class="box box-solid administrator">
            <div class="box-header with-border">
                <h3 class="box-title">Form Input Data Tenaga Kerja</h3>
            </div>
            <div class="box-body">
                <form class="manpower-form" action="<?= url('manpowers') ?>" method="post" >

                    <div class="hidden-input-placehoder">
                        {!! csrf_field() !!}    
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kualifikasi</label>
                                <input 
                                    type="text" 
                                    name="manpower_qualification" 
                                    class="form-control" 
                                    required="true"
                                    value='<?= old('manpower_qualification'); ?>'
                                    />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Koefisien</label>
                                <input 
                                    type="number" 
                                    step="0.1" 
                                    max="100" 
                                    min="0" 
                                    name="manpower_coeffisien" 
                                    class="form-control" 
                                    required="true" 
                                    value='<?= old('manpower_coeffisien'); ?>'
                                    />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="btn-placeholder btn-top-padding">
                                    <button type="submit" class="btn btn-info btn-add">
                                        <i class="fa fa-plus"></i> Tambah Tenaga Kerja
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border box-header-padding">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="box-title">Daftar Tenaga Kerja</h3>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input name="input_search" 
                                           type="text" 
                                           class="form-control" 
                                           placeholder="Masukkan kata kunci.. ." 
                                           aria-label="Search for..."
                                           value="<?= $input_search ?>"
                                           >
                                    <span class="input-group-btn">
                                        <button class="btn btn-search btn-default" type="button" >
                                            <i class="fa fa-search"></i> Cari Data
                                        </button>
                                        <a class="btn btn-default" href="<?= url('manpowers') ?>" >
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="manpowers-table" class="table table-borderless table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th width="30%" >
                                                Kualifikasi
                                            </th>
                                            <th width="10%" >
                                                Koefisien
                                            </th>
                                            <th width="15%" >
                                                UMK
                                            </th>
                                            <th width="15%" >
                                                Upah per Bulan
                                            </th>
                                            <th width="15%" >
                                                Upah per Jam
                                            </th>
                                            <th width="15%" ></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->manpower_qualification }}</td>
                                            <td>
                                                {{ $item->manpower_coeffisien }}
                                                <?php $manpowers->set($item->manpower_coeffisien); ?>
                                            </td>
                                            <td>Rp. {{ number_format($mrs_nominal,0,',', '.') }}</td>
                                            <td>Rp. <?= number_format($manpowers->month(1), 1, ',', '.'); ?></td>
                                            <td>Rp. <?= number_format($manpowers->hour(1), 1, ',', '.'); ?></td>
                                            <td>
                                                <div class="pull-right administrator">
                                                    <div class="btn-group">
                                                        <button 
                                                            class="btn btn-edit btn-success btn-xs" 
                                                            data-value='<?= json_encode($item); ?>' 
                                                            data-toggle="tooltip" 
                                                            data-placement="top" 
                                                            title="Ubah Data Konstruksi" >
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button 
                                                            class="btn btn-delete btn-danger btn-xs" 
                                                            data-toggle="tooltip" 
                                                            data-placement="top"
                                                            title="Hapus Data Material"
                                                            data-id="{{ $item->manpower_id }}"
                                                            >
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="page-link pull-right">
                                    {{ $data->appends(['search' => $input_search ])->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer with-border">
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@stop

@section('js')
@include('system.manpowers.js.index')
@stop