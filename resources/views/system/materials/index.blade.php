@extends('system.masters.layout')

@section('hidden_input')
<input type="hidden" name="base_url" value="<?= url('materials') ?>" />
@stop

@section('modal')

@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

        @include('system.masters.alert.error')
        @include('system.masters.alert.success')

        <div class="box box-solid administrator">
            <div class="box-header with-border">
                <h3 class="box-title">Form Input Data Material</h3>
            </div>
            <div class="box-body">
                <form class="materials-form" method="post" >

                    <div class="hidden-input-placehoder">
                        {!! csrf_field() !!}    
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Jenis Material</label>
                                <input 
                                    type="text" 
                                    name="material_name" 
                                    class="form-control" 
                                    value="<?= old('material_name'); ?>"
                                    />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Waktu Pemasangan ( Menit )</label>
                                <input 
                                    type="number" 
                                    step="0.1" 
                                    name="material_setup_time" 
                                    class="form-control" 
                                    value='<?= old('material_setup_time'); ?>'
                                    />
                                <span class="help-block hidden"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="btn-placeholder btn-top-padding">
                                    <button type="submit" class="btn btn-info btn-add">
                                        <i class="fa fa-plus"></i> Tambah Material
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border box-header-padding">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="box-title">Daftar Material</h3>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input name="input_search" 
                                           type="text" 
                                           class="form-control" 
                                           placeholder="Masukkan kata kunci.. ." 
                                           aria-label="Search for..."
                                           value="<?= $input_search ?>"
                                           >
                                    <span class="input-group-btn">
                                        <button class="btn btn-search btn-default" type="button" >
                                            <i class="fa fa-search"></i> Cari Data
                                        </button>
                                        <a class="btn btn-default" href="<?= url('materials') ?>" >
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table-materials" class="table table-borderless table-striped table-hover table-font-modif">
                                    <thead>
                                        <tr>
                                            <th width="40%" >
                                                Jenis Material
                                            </th>
                                            <th width="20%" >
                                                Waktu Pemasangan
                                            </th>
                                            <th width="40%" ></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->material_name }}</td>
                                            <td>{{ $item->material_setup_time }} Menit</td>
                                            <td>
                                                <div class="pull-right administrator">
                                                    <div class="btn-group">
                                                        <button class="btn btn-edit btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Ubah Data Konstruksi" data-value='<?= json_encode($item); ?>' >
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button class="btn btn-delete btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus Data Material" data-id='<?= $item->material_id; ?>' >
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="page-link pull-right">
                                    {{ $data->appends(['search' => $input_search ])->links() }}
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer with-border">
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@stop

@section('js')
@include('system.materials.js.index')
@stop