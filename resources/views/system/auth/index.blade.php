<html>
    <header>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Login to HSE App</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        @include('system.masters.css')

    </header>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <b>HSE</b> APP<br/>
                <span style="font-size: 16px; font-weight:bold;">PLN (PERSERO) </span><br/>
                <span style="font-size: 16px; font-weight:bold;" >DISTRIBUSI JAWA TENGAH DAN D.I. YOGYAKARTA</span><br/>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">

                @include('system.masters.alert.error')

                <p class="login-box-msg">Masukkan username dan password untuk masuk ke dalam sistem</p>

                <form action="<?= url('/'); ?>" method="post">

                    <div class="hidden-input-placehoder">
                        {!! csrf_field() !!}    
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" name="user_username" class="form-control" placeholder="Masukkan Username" required="true" >
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input  type="password" name="user_password" class="form-control" placeholder="Masukkan Password" required="true" >
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                    </div>

                </form>

            </div>
            <!-- /.login-box-body -->
        </div>

        @include('system.masters.js')

    </body>
</html>