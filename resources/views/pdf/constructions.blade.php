<html>
    <header>
        <style type="text/css">

            table {
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
            }

            table tr th {
                border-bottom : solid black 1px;
                border-top : solid black 1px;
                border-right : solid black 1px;
                border-left : solid black 1px;
                padding: 5px 5px 5px 5px;
                margin: 0px 0px 0px 0px;
                text-align: center;
            }

            table tr td {
                border-bottom : solid black 1px;
                border-top : solid black 1px;
                border-right : solid black 1px;
                border-left : solid black 1px;
                padding: 5px 5px 5px 5px;
                margin: 0px 0px 0px 0px;
            }

            .pull-right{
                text-align: right;
            }

            .body{
                width: 100%;
            }

            .header{
                width: 100%;
            }

            .logo {
                width: 50px;
            }

            .table-header tr td{
                border: none;
                padding: 3px 3px 3px 3px;
            }
            
            #table-header-format{
                font-weight: bold;
            }

        </style>
    </header>
    <body>
        <div class="header">
            <table class="table-header" >
                <tr>
                    <td style="width:50px;">
                        <img class="logo" src="<?= $_SERVER["DOCUMENT_ROOT"] . '/upload/logo.jpg'; ?>"/>        
                    </td>
                    <td>
                        <table class="table-header" id="table-header-format">
                            <tr>
                                <td>PT. PLN (PERSERO)</td>
                            </tr>
                            <tr>
                                <td>DISTRIBUSI JAWA TENGAH DAN D.I. YOGYAKARTA</td>
                            </tr>
                            <tr>
                                <td>AREA SEMARANG</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <hr/>
        </div>
        <div class="body">
            <div style="text-align: center;">
                <h3>DAFTAR BIAYA KONSTRUKSI</h3>
            </div>
            <div>
                <table> 
                    <thead >
                        <tr>
                            <th>Jenis Konstruksi</th>
                            <th>Tenaga Kerja per Jam</th>
                            <th>Peralatan per Jam</th>
                            <th>WPM*</th>
                            <th>Total Biaya per Konstruksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $item)
                        <tr>
                            <td >
                                {{ $item->construction_name }}
                            </td>
                            <td class="pull-right">
                                {{ number_format($item->manpower_cost, 2 , ',' , '.') }}
                            </td>
                            <td class="pull-right">
                                {{ number_format($item->equipment_cost , 2 , ',' , '.') }}
                            </td>
                            <td class="pull-right" >
                                {{ ($item->wpm)?$item->wpm:'-' }}
                            </td>
                            <td class="pull-right" >
                                {{ number_format($item->total_cost, 2 , ',' , '.') }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div style="margin-top: 10px;">
                *WPM (Waktu Pemasangan Material per Konstruksi dalam menit)
            </div>
        </div>
    </body>
</html>