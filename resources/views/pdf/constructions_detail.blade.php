<html>
    <header>
        <style type="text/css">

            table {
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
            }

            table tr th {
                border-bottom : solid black 1px;
                border-top : solid black 1px;
                border-right : solid black 1px;
                border-left : solid black 1px;
                padding: 5px 5px 5px 5px;
                margin: 0px 0px 0px 0px;
                text-align: center;
            }

            table tr td {
                border-bottom : solid black 1px;
                border-top : solid black 1px;
                border-right : solid black 1px;
                border-left : solid black 1px;
                padding: 5px 5px 5px 5px;
                margin: 0px 0px 0px 0px;
            }

            .pull-right{
                text-align: right;
            }

            .body{
                width: 100%;
            }

            .header{
                width: 100%;
            }

            .logo {
                width: 50px;
            }

            .table-header tr td{
                border: none;
                padding: 3px 3px 3px 3px;
            }

            #table-header-format{
                font-weight: bold;
            }

            .table-title tr th{
                border: none;
                text-align: left;
            }

            .row{
                margin-top: 20px;
            }

            .th-title{
                background-color: #d1d1d1; 
            }

            .table-content{
                font-size: 12px;
            }

            .text-pull-left{
                text-align: left;
            }

            .text-pull-right{
                text-align: right;
            }

            .text-pull-center{
                text-align: center;
            }

            .header-size{
                font-size: 14px;
            }

        </style>
    </header>
    <body >
        <div class="header">
            <table class="table-header" >
                <tr>
                    <td style="width:50px;">
                        <img class="logo" src="<?= $_SERVER["DOCUMENT_ROOT"] . '/upload/logo.jpg'; ?>"/>        
                    </td>
                    <td>
                        <table class="table-header" id="table-header-format">
                            <tr>
                                <td>PT. PLN (PERSERO)</td>
                            </tr>
                            <tr>
                                <td>DISTRIBUSI JAWA TENGAH DAN D.I. YOGYAKARTA</td>
                            </tr>
                            <tr>
                                <td>AREA SEMARANG</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <hr/>
        </div>
        <div class="body">
            <div style="text-align: center; padding: 0px 0px 0px 0px;">
                <h3>DETAIL BIAYA KONSTRUKSI</h3>
            </div>
            <div>
                <table class="table-title"> 
                    <thead class="header-size" >
                        <tr>
                            <th width="20%" >Jenis Konstruksi</th>
                            <th width="1%" >:</th>
                            <th width="79%" >{{ $data['construction_name'] }}</th>
                        </tr>
                        <tr>
                            <th>Total Biaya </th>
                            <th>:</th>
                            <th>Rp. {{number_format($data['total_cost'], 2, ',','.')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="row">
                <table > 
                    <thead class="table-content" >
                        <tr>
                            <th class="th-title" colspan="5" >
                                TENAGA KERJA
                            </th>
                        </tr>
                        <tr >
                            <th width="30%" >Tenaga Kerja</th>
                            <th width="15%" >Koefisien</th>
                            <th width="20%" >Biaya / Jam</th>
                            <th width="15%" >Durasi Kerja</th>
                            <th width="20%" >Total Biaya</th>
                        </tr>
                    </thead>
                    <tbody class="table-content" >
                        <?php $manpower_total = 0; ?>
                        @foreach($data['manpercons'] as $item)
                        <tr>
                            <td>{{$item['manpowers']['manpower_qualification']}}</td>
                            <td class="text-pull-center" >{{$item['manpercons_coefficient']}}</td>
                            <td class="text-pull-right" >Rp. {{ number_format($item['manpowers']['manpower_hourlycost'],2,',','.') }}</td>
                            <td class="text-pull-center" >{{$item['manpercons_work_time'] }} Jam</td>
                            <td class="text-pull-right " >Rp. {{ number_format(($item['manpowers']['manpower_hourlycost']*$item['manpercons_coefficient'])*$item['manpercons_work_time'],2,',','.') }}</td>
                        </tr>
                        <?php $manpower_total += ($item['manpowers']['manpower_hourlycost'] * $item['manpercons_coefficient']) * $item['manpercons_work_time']; ?>
                        @endforeach
                    </tbody>
                    <tfoot class="table-content" >
                        <tr class="th-title"  >
                            <th colspan="4" class="text-pull-left" >TOTAL BIAYA TENAGA KERJA</th>
                            <th class="text-pull-right ">Rp. <?= number_format($manpower_total, 2, ',', '.'); ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="row">
                <table > 
                    <thead class="table-content" >
                        <tr>
                            <th class="th-title" colspan="4" >
                                PERALATAN
                            </th>
                        </tr>
                        <tr>
                            <th width="45%" >Perlengkapan</th>
                            <th width="20%" >Biaya / Jam</th>
                            <th width="15%" >Waktu<br/> Pemakaian (Jam)</th>
                            <th width="20%" >Total Biaya</th>
                        </tr>
                    </thead>
                    <tbody class="table-content" >
                        <?php $equipment_total = 0; ?>
                        @foreach($data['equipercons'] as $item)
                        <tr>
                            <td>{{ $item['equipments']['equipment_type'] }}</td>
                            <td class="text-pull-right" >Rp. {{ number_format($item['equipments']['equipment_cost'],2,',','.') }}</td>
                            <td class="text-pull-center" >{{ $item['equipercons_use_time'] }} Jam</td>
                            <td class="text-pull-right" >Rp. {{ number_format($item['equipments']['equipment_cost']* $item['equipercons_use_time'],2,',','.') }}</td>
                        </tr>
                        <?php $equipment_total += ($item['equipments']['equipment_cost'] * $item['equipercons_use_time']); ?>
                        @endforeach
                    </tbody>
                    <tfoot class="table-content" >
                        <tr class="th-title"  >
                            <th colspan="3" class="text-pull-left" >
                                TOTAL BIAYA PERALATAN / KONSTRUKSI
                            </th>
                            <th class="text-pull-right ">Rp. <?= number_format($equipment_total, 2, ',', '.'); ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="row">
                <table > 
                    <thead class="table-content" >
                        <tr>
                            <th class="th-title" colspan="4" >
                                MATERIAL
                            </th>
                        </tr>
                        <tr>
                            <th width="45%" >Material</th>
                            <th width="20%" >Waktu <br/> Pemasangan</th>
                            <th width="15%" >Jumlah <br/> Material</th>
                            <th width="20%" >Total Waktu <br/> Pemasangan</th>
                        </tr>
                    </thead>
                    <tbody class="table-content" >
                        <?php $material_total = 0; ?>
                        @foreach($data['matpercons'] as $item)
                        <tr>
                            <td>{{ $item['materials']['material_name'] }}</td>
                            <td class="text-pull-center" >{{ $item['materials']['material_setup_time'] }} Jam</td>
                            <td class="text-pull-center" >{{ $item['matpercons_quantity'] }}</td>
                            <td class="text-pull-center" >{{ ($item['materials']['material_setup_time'] * $item['matpercons_quantity']) }} Jam</td>
                            <?php $material_total += ($item['materials']['material_setup_time'] * $item['matpercons_quantity']); ?>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot class="table-content" >
                        <tr class="th-title"  >
                            <th colspan="3" class="text-pull-left" >
                                TOTAL WAKTU PEMASANGAN
                            </th>
                            <th ><?= round($material_total/60,2) ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>
    </body>
</html>