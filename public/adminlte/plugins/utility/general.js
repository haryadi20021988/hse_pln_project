function show_modal_delete(base_url) {

    // Open delete modal confirmation
    $('body').on('click', '.btn-delete', function () {

        // Get id 
        var id = $(this).attr('data-id');

        // Variable initialization 
        var url = base_url + '/' + id;

        // Append url into modal
        $('#modal-delete-confirmation').find('form').attr('action', url);

        // Open delete modal confirmation
        $('#modal-delete-confirmation').modal({
            backdrop: 'static',
            keyboard: false
        });

    });
    
}