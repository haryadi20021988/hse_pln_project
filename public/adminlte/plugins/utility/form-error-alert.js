function show_alert(key, message) {

    // Variable initialization
    var html = $('input[name="' + key + '"]').parent();

    // Show error alert
    $(html).addClass('has-error');

    // Set error message
    $(html).find('.help-block').text(message);

    // Show error message
    $(html).find('.help-block').removeClass('hidden');

}

function reset_form_group() {

    // Variable initialization
    var tag = $('.form-group');

    // Remove error notification
    $(tag).removeClass('has-error');

    // Clean error note text
    $(tag).find('.help-block').text('');

    // Hide error note
    $(tag).find('.help-block').addClass('hidden');

}

function clean_input_value() {

    // Variable initialization
    var tag = $('.form-group');

    // Get input
    $(tag).find('input').not('input[type="radio"]').val('');

    // Remove all checked radio
    $(tag).find('input[type="radio"]').removeAttr('checked');

    // Set default value
    $(tag).find('input.radio-default').prop('checked', true);

}

function populate_data(data) {

    // Variable intialization
    var data = JSON.parse(data);

    // Break down data 
    $.each(data, function (index, value) {

        // Get type of input form
        var type = $('input[name="' + index + '"]').attr('type');

        // If type is text
        if (type == 'text' || type == 'email') {
            $('input[name="' + index + '"]').val(value);
        }

        // If type is radio
        if (type == 'radio') {
            $('input[name="' + index + '"]').each(function (i, v) {
                if($(this).val() == value){
                    $(this).prop('checked', true);
                }
            });
        }

    });

}