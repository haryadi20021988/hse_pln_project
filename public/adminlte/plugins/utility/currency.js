function formating(value) {

    // Variable initialization
    var str = value.toString();
    var base = value.toString();
    var decimal = '';
    var float = '';
    var result = '';

    // Split string into array
    base = base.split('.');

    // Get floating number
    if (str.indexOf('.') > -1) {
        float = ',' + base[1];
    }

    // Get decimal number
    decimal = base[0];

    // Convert decimal number into array
    decimal = decimal.split('');

    // Break down 
    var key = decimal.length;
    $.each(decimal, function (index, val) {

        if (((key) % 3 == 0) && (index != 0)) {
            result += '.';
        }

        result += val;

        key--;


    });

    // Merge decimal and floating number
    result = result  + float;

    // REturn result complete number
    return result;

}