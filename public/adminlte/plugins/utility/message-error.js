function error_message(input, message) {

    // Get parent dom
    var dom = $(input).parent();

    // Set error message
    $(dom).find('.help-block').text(message);

    // Show error sign
    $(dom).addClass('has-error');

}

function clear_error_message() {

    // Select dom
    var dom = $('body').find('.has-error');

    // Remove all error sign
    $(dom).each(function (index, value) {

        // Remove one by one
        $(value).removeClass('has-error');

        // Remove help-doc
        $(value).find('.help-block').empty();

    });

}

function table_alert(message) {

    // Variable initialiation
    var html = '<div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">'
            + '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a> '
            + message
            + ' </div>';
    
    // Append alert into placeholder
    $('.table-alert').append(html);
    
}

