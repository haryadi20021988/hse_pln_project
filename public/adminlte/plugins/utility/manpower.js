function manpower_total_cost(coeffisien, cost_perhour, work_time) {
    return (coeffisien * cost_perhour) * work_time;
}

function equipment(price, lifetime = 12, round = 2) {

    // Variable initialization
    var result = 0;
    var interest_rate = $('input[name="interest_rate"]').val();

    // Get interest rate
    var ir = interest_rate / 100;

    // Calculation
    var d1 = ir * Math.pow((1 + ir), lifetime);
    var d2 = Math.pow((1 + ir), lifetime) - 1;
    result = price * (d1 / d2) / (22 * 7);

    // Return result
    return result.toFixed(round);

}