<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::resource('/', 'Systems\AuthController');
Route::get('logout', 'Systems\AuthController@logout');

Route::group(['prefix' => '/', 'middleware' => 'sysauth'], function() {

    Route::resource('users', 'Systems\UsersController');
    Route::resource('allowances', 'Systems\AllowancesController');
    Route::resource('constructions', 'Systems\ConstructionsController');
    Route::resource('equipments', 'Systems\EquipmentsController');
    Route::resource('materials', 'Systems\MaterialsController');
    Route::resource('manpowers', 'Systems\ManPowersController');
    Route::resource('minimum-regional-salary', 'Systems\MinimumRegionalSalaryController');

    // Transaction router
    Route::resource('matpercons', 'Systems\MatperconsController');
    Route::resource('manpercons', 'Systems\ManperconsController');
    Route::resource('equipercons', 'Systems\EquiperconsController');
    
    // Profile
    Route::resource('profile', 'Systems\ProfileControllers');

    // Calculator router 
    Route::get('manpower-cost', 'Systems\CalcController@manpower_cost');

    // Download pdf all construction report
    Route::get('download-all-report', 'Systems\PdfPrintController@all_constructions_report');

    // Download pdf detail construction report 
    Route::get('download-detail-report/{id}', 'Systems\PdfPrintController@detail_constructions_report');
    
});

