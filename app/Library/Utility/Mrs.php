<?php

namespace App\Library\Utility;

use App\Model\System\MinimumRegionalSalary;

class Mrs {

    public $mrs_id;
    public $mrs_year;
    public $mrs_nominal;

    public function __construct() {
        $mrs = MinimumRegionalSalary::where('mrs_status', '=', 1)->first();
        $this->mrs_id = $mrs->mrs_id;
        $this->mrs_year = $mrs->mrs_year;
        $this->mrs_nominal = $mrs->mrs_nominal;
    }

    public static function init() {
         $object = new self;
        return $object;
    }


}
