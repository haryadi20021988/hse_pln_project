<?php

namespace App\Library\Utility;

use Illuminate\Support\Facades\Facade;

class AllowanceFormaterFacades extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'AllowanceFormater';
    }

}
