<?php

namespace App\Library\Utility;

use App\Model\System\Allowances;

class EquipmentCalc {

    private static $result;

    public static function init($price, $lifetime = 12) {

        // Get interest rate
        $ir = config('setting.interest_rate') / 100;

        // Calculation
        $d1 = $ir * pow((1 + $ir), $lifetime);
        $d2 = pow((1 + $ir), $lifetime) - 1;
        self::$result = $price * ($d1 / $d2) / (22 * 7);

        // Return result
        $object = new self;
        return $object;
        
    }

    public static function get($round = 0) {
        return round(self::$result, $round);
    }

}
