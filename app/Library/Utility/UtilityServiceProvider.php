<?php

namespace App\Library\Utility;

use Illuminate\Support\ServiceProvider;

class UtilityServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {

        // Register check library
        $this->app->bind('allowanceFormater', function() {
            return new \App\Library\Utility\AllowanceFormater;
        });
        
        // Register check library
        $this->app->bind('manpowerCalculation', function() {
            return new \App\Library\Utility\ManpowerCalculation;
        });
        
    }

}
