<?php

namespace App\Library\Utility;

use App\Model\System\Allowances;
use Brick\Math\BigDecimal;

class AllowanceFormater {

    public static function get() {

        // Variable initialization
        $result = array();

        // Get data from database
        $data = Allowances::all();

        // Format data into pair of key and value
        foreach ($data as $key => $value) {

            $result[$value->allowance_key] = $value->allowance_value;
        }

        // Return ramapping data
        return $result;
    }

    public static function total() {

        // Variable initialization
        $total = 0;
        $allowances = self::get();

        // Calculate all allowances
        foreach ($allowances as $key => $item) {
            $total = BigDecimal::of($total + ($item / 100))->toFloat();
        }

        // Return total allowances has calculated
        return BigDecimal::of($total + 1)->toFloat();
        
    }

}
