<?php

namespace App\Library\Utility;

use App\Model\System\Allowances;
use App\Library\Utility\AllowanceFormater;
use App\Model\System\MinimumRegionalSalary;

class ManpowerCalculation {

    private static $coefisien;
    private static $umk;

    public function __construct() {
        $umk = MinimumRegionalSalary::where('mrs_status', '=', 1)->first();
        self::$umk = $umk->mrs_nominal;
    }

    public function set($coefisien_) {

        // Property first initialization 
        self::$coefisien = $coefisien_;

        // Return object it self
        $object = new self;
        return $object;
        
    }

    public function month($round = null) {

        // Return  manpower calculation
        $operand = self::$coefisien * self::$umk;
        $operand = $operand * AllowanceFormater::total();

        // Return the result
        return $round == null ? $operand : round($operand, $round);
        
    }

    public function hour($round = null) {

        // Variable initialization
        $result = self::month() / 22;
        $result = $result / 7;

        // Return result 
        return $round == null ? $result : round($result, $round);
        
    }

}
