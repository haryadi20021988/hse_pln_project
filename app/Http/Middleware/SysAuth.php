<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Auth;
use App\Library\SysLog\SysLog;
use Illuminate\Support\Facades\DB;

class SysAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if (session('passport')) {
            return $next($request);
        } else {
            return redirect('/');
        }

    }

    public function terminate($request, $response) {
        
    }

}
