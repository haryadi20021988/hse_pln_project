<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\System\Users;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        // Return login page
        return view('system.auth.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Get active user from dataabse that match with username inputted
        $account = Users::where('user_username', '=', $request->user_username)
                ->where('user_status', '=', 1)
                ->first();

        // If password match with user inputted
        if ($account && Hash::check($request->user_password, $account->user_password)) {

            // Save user data account into session
            session(['passport' => $account]);

            // Redirect into construction page
            return redirect('constructions');
            
        } else {

            // If user login false redirect into login page
            return redirect('/')->with('error-alert', 'Username dan password Anda tidak terdaftar');
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout() {
        
        // Remove account session
        session()->flush();

        // Redirect into login page
        return redirect('/');
        
    }

}
