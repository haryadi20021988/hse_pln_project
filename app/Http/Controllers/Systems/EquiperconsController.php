<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Uuid;
use App\Model\System\Equipercons;

class EquiperconsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Variable initialization
        $data = null;

        // Remove data that deleted
        if ($request->equipercons_deleted) {
            $this->destroy($request);
        }

        // Break down material item
        foreach ($request->equipment_items as $key => $item) {

            // Convert json string into php array
            $item = json_decode($item, true);

            // Check is new or update
            if (!$item['equipercons_id']) {

                // Create new instance
                $data = new Equipercons();
                $data->equipercons_id = Uuid::generate();
            } else {

                // Find data
                $data = Equipercons::find($item['equipercons_id']);
            }

            // Collecting data 
            $data->equipercons_construction_id = $item['equipercons_construction_id'];
            $data->equipercons_equipment_id = $item['equipercons_equipment_id'];
            $data->equipercons_cost = $item['equipercons_totalcost'];
            $data->equipercons_use_time = $item['equipercons_use_time'];

            // Save instance
            $data->save();
        }

        // Redirect into construction page
        return redirect('constructions')->with('success-alert', 'Data peralatan berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        // Get data from database
        return Equipercons::where('equipercons_construction_id', '=', $id)
                        ->join('equipments', 'equipercons.equipercons_equipment_id', '=', 'equipments.equipment_id')
                        ->with('equipments')
                        ->orderBy('equipments.equipment_type','asc')
                        ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($request) {

        // Variable initialization
        $items = $request->equipercons_deleted;

        // Convert json string into php array
        $items = json_decode($items, true);

        // Break down material items
        foreach ($items as $key => $item) {

            // Delete items due
            $data = Equipercons::find($item);
            $data->delete();
        }

        // Return true
        return true;
    }

}
