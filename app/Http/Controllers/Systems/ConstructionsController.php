<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Uuid;
use App\Model\System\Constructions;
use App\Model\System\Materials;
use App\Model\System\Equipments;
use App\Model\System\Manpowers;
use App\Library\Utility\ManpowerCalculation;
use App\Library\Utility\EquipmentCalc;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ConstructionsController extends Controller {

    // Local private property
    private $base_url = 'constructions';
    private $object;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->object = new Constructions();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        // Variable initialization
        $send['input_search'] = '';

        // Construct select string
        $str = 'constructions.construction_id, '
                . ' constructions.construction_name,'
                . ' TotalEquipmentCost(constructions.construction_id) as equipment_cost,'
                . ' ROUND((TotalMaterialTime(constructions.construction_id)/60),2) as wpm,'
                . ' TotalManpowerCost(constructions.construction_id) as manpower_cost,'
                . ' ROUND('
                . ' (TotalEquipmentCost(constructions.construction_id) '
                . ' + TotalManpowerCost(constructions.construction_id)) * '
                . ' ROUND((TotalMaterialTime(constructions.construction_id)/60),2)'
                . ' ,2) as total_cost';

        // Get data from data base
        $data_temp = Constructions::select(DB::raw($str));

        // Do search if there have keyword
        if (Input::get('search')) {
            
            // Where query build up
            $data_temp = $data_temp->where('constructions.construction_name', 'like', '%' . Input::get('search') . '%');
            
            // Set up search key word
            $send['input_search'] = Input::get('search');
            
        }

        // Get data from database
        $send['data'] = $data_temp->orderBy('construction_name', 'asc')->paginate(10);

        // Get data material from data base
        $send['materials'] = Materials::orderBy('material_name', 'asc')->get();

        // Get data man power from data-base
        $send['manpowers'] = Manpowers::orderBy('manpower_qualification', 'asc')->get();
        $send['manpowers'] = $send['manpowers']->load('minimum_regional_salary');

        // Get data equipments from database
        $send['equipments'] = Equipments::orderBy('equipment_type', 'asc')->get();

        // Calculator Object
        // Send manpower calculator
        $send['manpowercalc'] = new ManpowerCalculation();

        // Send equipment calculator
        $send['equipmentcalc'] = new EquipmentCalc;

        // Return data into index page
        return view('system.constructions.index', $send);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Create new object users
        $data = new $this->object;

        // Validation
        $validator = $this->validateStore($request);
        if ($validator->fails()) {
            return redirect($this->base_url)->with('error', $validator->errors())->withInput();
        }

        // Collecting data
        $this->collectRequest($request);

        // Return data
        return redirect($this->base_url)->with('success-alert', 'Data konstruksi berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        // Get data from data base
        $data = Constructions::select(DB::raw('*, ROUND('
                                . ' (TotalEquipmentCost(constructions.construction_id) '
                                . ' + TotalManpowerCost(constructions.construction_id)) * '
                                . ' ROUND((TotalMaterialTime(constructions.construction_id)/60),2)'
                                . ' ,2) as total_cost'))
                ->where('construction_id', '=', $id)
                ->first();

        // Load material details
        $data = $data->load('matpercons.materials');

        // Load equipment details
        $data = $data->load('equipercons.equipments');

        // Load manpowers details
        $data = $data->load('manpercons.manpowers');

        // Return data as json
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        // Validation
        $validator = $this->validateStore($request, true);

        // Return validation if fails
        if ($validator->fails()) {
            return redirect($this->base_url)->with('error', $validator->errors())->withInput();
        }

        // Store request data to object
        $this->collectRequest($request, $id);

        // Return success message
        return redirect($this->base_url)->with('success-alert', 'Data konstruksi berhasil di diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        // Get detail users
        $data = $this->object->find($id);

        // Check is users exist
        if ($data) {

            $data->matpercons()->delete();

            $data->manpercons()->delete();

            $data->equipercons()->delete();

            // Last delete users if exist
            $data->delete();

            // Return success into data page
            return redirect($this->base_url)->with('success-alert', 'Hapus data konstruksi berhasil dilakukan');
        } else {

            // Return failed into data page
            return redirect($this->base_url)->with('error-alert', 'Hapus data konstruksi gagal dilakukan');
        }
    }

    /**
     * Collect request and save to Properties Details object.
     *
     * @param  obj  $customerDetails
     * @param  request  $request
     * @return obj  $customerDetails
     */
    private function collectRequest($request, $id = '') {

        // Switch is request create or update
        if ($id == '') {

            // Create data
            $data = new $this->object;
            $data->construction_id = Uuid::generate();
        } else {

            // Edit data
            $data = $this->object->find($id);
        }

        // Data collecting
        $data->construction_name = $request->construction_name;

        // Save data
        $data->save();

        // Return true
        return true;
    }

    /**
     * Validate store customer request.
     *
     * @param  request  $request
     * @return obj  $customer
     */
    private function validateStore($request, $edit = false) {

        $value = [
            'construction_name' => 'required|max:100',
        ];

        $validator = Validator::make($request->all(), $value);

        return $validator;
    }

}
