<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\System\Manpowers;
use App\Library\Utility\ManpowerCalculation;
use App\Library\Utility\Mrs;
use Validator;
use Uuid;
use Illuminate\Support\Facades\Input;

class ManPowersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        // Variable intialization
        $send['input_search'] = '';

        // Get data from database
        $data_temp = Manpowers::select('*');

        // Do search if search keyword is available
        if (Input::get('search')) {
            
            // Build where query 
            $data_temp = $data_temp->where('manpower_qualification', 'like', '%' . Input::get('search') . '%');
            
            // Set up search key word
            $send['input_search'] = Input::get('search');
            
        }
        
        // Get data from data base query 
        $send['data'] = $data_temp->orderBy('manpower_coeffisien', 'desc')->paginate(10);

        // Set data man power calculation
        $send['manpowers'] = new ManpowerCalculation();

        // Set data minimun regional salary
        $send['mrs_nominal'] = Mrs::init()->mrs_nominal;

        // Return data as index page
        return view('system.manpowers.index', $send);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Create new object users
        $data = new Manpowers;

        // Validation
        $validator = $this->validateStore($request);
        if ($validator->fails()) {
            return redirect('manpowers')->with('error', $validator->errors())->withInput();
        }

        // Collecting data
        $this->collectRequest($request);

        // Return data
        return redirect('manpowers')->with('success-alert', 'Data tenaga kerja berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        // Validation
        $validator = $this->validateStore($request, true);

        // Return validation if fails
        if ($validator->fails()) {
            return redirect('manpowers')->with('error', $validator->errors())->withInput();
        }

        // Store request data to object
        $this->collectRequest($request, $id);

        // Return success message
        return redirect('manpowers')->with('success-alert', 'Data tenaga kerja berhasil di diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        // Get detail users
        $data = Manpowers::find($id);

        // Check is users exist
        if ($data) {

            // Last delete users if exist
            $data->delete();

            // Return success into data page
            return redirect('manpowers')->with('success-alert', 'Hapus data tenaga kerja berhasil dilakukan');
        } else {

            // Return failed into data page
            return redirect('manpowers')->with('error-alert', 'Hapus data tenaga kerja gagal dilakukan');
        }
    }

    /**
     * Collect request and save to Properties Details object.
     *
     * @param  obj  $customerDetails
     * @param  request  $request
     * @return obj  $customerDetails
     */
    private function collectRequest($request, $id = '') {

        // Switch is request create or update
        if ($id == '') {

            // Create data
            $data = new Manpowers;
            $data->manpower_id = Uuid::generate();
        } else {

            // Edit data
            $data = Manpowers::find($id);
        }

        // Data collecting
        $data->manpower_qualification = $request->manpower_qualification;
        $data->manpower_coeffisien = $request->manpower_coeffisien;
        $data->manpower_mrs_id = Mrs::init()->mrs_id;

        // Save data
        $data->save();

        // Return true
        return true;
    }

    /**
     * Validate store customer request.
     *
     * @param  request  $request
     * @return obj  $customer
     */
    private function validateStore($request, $edit = false) {

        $value = [
            'manpower_qualification' => 'required',
            'manpower_coeffisien' => 'required|numeric|min:0|max:10'
        ];

        $validator = Validator::make($request->all(), $value);

        return $validator;
    }

}
