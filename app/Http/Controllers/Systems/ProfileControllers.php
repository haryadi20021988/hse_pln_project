<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Uuid;
use Illuminate\Support\Facades\Hash;
use App\Model\System\Users;

class ProfileControllers extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        // User detai from database
        $data = Users::find($id);

        // Set data to send
        $data['data'] = $data;

        // Return redirect into edit profile page
        return view('system.profiles.index', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        // Get detail user to check is exist already 
        $isExist = Users::where('user_email', '=', $request->user_email)
                ->orWhere('user_username', '=', $request->user_username);

        // Get count of data existed
        $count = clone $isExist;
        $count = $count->count();

        // Get first data
        $first = $isExist->first();

        // Check whether email is exist or not
        if ($count > 1 || ($count == 1 && $first->user_id != $id))
            return redirect('profile/' . $id . '/edit')->with('error-alert', 'Email atau username sudah digunakan')->withInput();

        // Check user type
        if ($request->user_type == 'Staff' || $request->user_status == 0) {

            // Check is super admin is more than one
            $super_admin = Users::where('user_type', '=', 'Administrator')
                    ->where('user_status', '=', 1)
                    ->get();

            // Check wheter adminstrator is hava minimal one
            if (count($super_admin) < 2)
                return redirect('profile/' . $id . '/edit')->with('error-alert', 'Harus ada minimal satu pengguna aktif sebagai administrator');
            
        }

        // Validation
        $validator = $this->validateStore($request, true);

        // Return validation if fails
        if ($validator->fails())
            return redirect('profile/' . $id . '/edit')->with('error', $validator->errors())->withInput();

        // Get customer and details data
        $data = Users::find($id);

        // Store request data to object
        $data = $this->collectRequest($data, $request, $id);

        // Store data to databases
        $data->save();

        // Return success message
        return redirect('profile/' . $id . '/edit')->with('success-alert', 'Data user berhasil di diperbarui');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * Collect request and save to Properties Details object.
     *
     * @param  obj  $customerDetails
     * @param  request  $request
     * @return obj  $customerDetails
     */
    private function collectRequest($data, $request, $id = '') {

        // Switch is request create or update
        if ($id == '') {

            // Create data 
            $data = new Users;
            $data->user_id = Uuid::generate();
        } else {

            // Edit data
            $data = Users::find($id);
        }

        // Data collecting
        $data->user_fullname = $request->user_fullname;
        $data->user_email = $request->user_email;
        $data->user_username = $request->user_username;
        $data->user_nip = $request->user_nip;

        // Change new password if user inputted
        if ($request->user_password)
            $data->user_password = Hash::make($request->user_password);

        // Return  as object
        return $data;
    }

    /**
     * Validate store customer request.
     *
     * @param  request  $request
     * @return obj  $customer
     */
    private function validateStore($request, $edit = false) {

        $value = [
            'user_fullname' => 'required|max:50',
            'user_email' => 'required|email|max:50',
            'user_username' => 'required|max:50',
            'user_nip' => 'required|max:20'
        ];

        if ($edit == false) {
            $value['user_password'] = 'required';
        }

        $validator = Validator::make($request->all(), $value);

        return $validator;
    }

}
