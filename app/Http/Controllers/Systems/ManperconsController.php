<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Uuid;
use App\Model\System\Manpercons;

class ManperconsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        // Variable initialization
        $data = null;

        // Remove data that deleted
        if ($request->manpercons_deleted) {
            $this->destroy($request);
        }

        // Break down material item
        foreach ($request->manpower_items as $key => $item) {

            // Convert json string into php array
            $item = json_decode($item, true);

            // Check is new or update
            if (!$item['manpercons_id']) {

                // Create new instance
                $data = new Manpercons();
                $data->manpercons_id = Uuid::generate();
            } else {

                // Find data
                $data = Manpercons::find($item['manpercons_id']);
            }

            // Collecting data 
            $data->manpercons_construction_id = $item['manpercons_construction_id'];
            $data->manpercons_manpower_id = $item['manpercons_manpower_id'];
            $data->manpercons_coefficient = $item['manpercons_coefficient'];
            $data->manpercons_salary_perhour = $item['manpercons_salary_perhour'];
            $data->manpercons_total_cost = $item['manpercons_total_cost'];
            $data->manpercons_work_time = $item['manpercons_work_time'];

            // Save instance
            $data->save();
        }

        // Redirect into construction page
        return redirect('constructions')->with('success-alert', 'Data tenaga kerja berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        // Get data from database
        return Manpercons::where('manpercons_construction_id', '=', $id)
                ->join('manpowers', 'manpercons.manpercons_manpower_id', '=', 'manpowers.manpower_id')
                ->with('manpowers')
                ->orderBy('manpowers.manpower_qualification', 'asc')
                ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($request) {

        // Variable initialization
        $items = $request->manpercons_deleted;

        // Convert json string into php array
        $items = json_decode($items, true);

        // Break down material items
        foreach ($items as $key => $item) {

            // Delete items due
            $data = Manpercons::find($item);
            $data->delete();
            
        }

        // Return true
        return true;
        
    }

}
