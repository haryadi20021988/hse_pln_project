<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Uuid;
use App\Model\System\Matpercons;

class MatperconsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Variable initialization
        $data = null;

        // Remove data that deleted
        if ($request->matpercons_deleted) {
            $this->destroy($request);
        }

        // Break down material item
        foreach ($request->material_items as $key => $item) {

            // Convert json string into php array
            $item = json_decode($item, true);

            // Check is new or update
            if (!$item['matpercons_id']) {

                // Create new instance
                $data = new Matpercons();
                $data->matpercons_id = Uuid::generate();
            } else {

                // Find data
                $data = Matpercons::find($item['matpercons_id']);
            }

            // Collecting data 
            $data->matpercons_construction_id = $request->matpercons_construction_id;
            $data->matpercons_material_id = $item['matpercons_material_id'];
            $data->matpercons_quantity = $item['matpercons_quantity'];

            // Save instance
            $data->save();
        }

        // Redirect into construction page
        return redirect('constructions')->with('success-alert', 'Data material berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        // Get data from database
        return Matpercons::where('matpercons_construction_id', '=', $id)
                ->join('materials', 'matpercons.matpercons_material_id', '=', 'materials.material_id')
                ->with('materials')
                ->orderBy('materials.material_name', 'asc')
                ->get();
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($request) {

        // Variable initialization
        $items = $request->matpercons_deleted;

        // Convert json string into php array
        $items = json_decode($items, true);

        // Break down material items
        foreach ($items as $key => $item) {

            // Delete items due
            $data = Matpercons::find($item);
            $data->delete();
            
        }

        // Return true
        return true;
    }

}
