<?php

namespace App\Http\Controllers\Systems;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Uuid;
use Illuminate\Support\Facades\Hash;
use App\Model\System\Users;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        // Variable intialization
        $send['input_search'] = '';

        // Get data form database
        $data_temp = Users::where('user_type', '<>', 'master');

        // Do search if search keyword is available
        if (Input::get('search')) {

            // Build where query 
            $data_temp = $data_temp->where(DB::raw("CONCAT(user_fullname , ' ' , user_email, ' ', user_username, ' ', user_nip)"), 'like', '%' . Input::get('search') . '%');

            // Set up search key word
            $send['input_search'] = Input::get('search');
            
        }

        // Get data from database query 
        $send['data'] = $data_temp->orderBy('user_fullname', 'asc')
                ->paginate(10);

        // Redirect to index page and send data
        return view('system.users.index', $send);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Get detail user to check is exist already TODO 
        $isEmailExist = Users::where('user_email', '=', $request->user_email)
                ->orWhere('user_username', '=', $request->user_username);

        // Check whether email is exist or not
        if ($isEmailExist->count() > 0) {

            // Return email is exist 
            return redirect('users')->with('error-alert', 'Email atau username sudah terdaftar')->withInput();
        }

        // Create new object users
        $data = new Users;

        // Validation 
        $validator = $this->validateStore($request);
        if ($validator->fails()) {
            return redirect('users')->with('error', $validator->errors())->withInput();
        }

        // Collecting data
        $data = $this->collectRequest($data, $request);

        // Save data
        $data->save();

        // Return data 
        return redirect('users')->with('success-alert', 'Data user berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        // Get detail user to check is exist already 
        $isExist = Users::where('user_email', '=', $request->user_email)
                ->orWhere('user_username', '=', $request->user_username);

        // Get count of data existed
        $count = clone $isExist;
        $count = $count->count();

        // Get first data
        $first = $isExist->first();

        // Check whether email is exist or not
        if ($count > 1 || ($count == 1 && $first->user_id != $id)) {

            // Return email is exist 
            return redirect('users')->with('error-alert', 'Email atau username sudah digunakan')->withInput();
        }

        // Check user type
        if ($request->user_type == 'Staff' || $request->user_status == 0) {

            // Check is super admin is more than one
            $super_admin = Users::where('user_type', '=', 'Administrator')
                    ->where('user_status', '=', 1)
                    ->get();

            // Check wheter adminstrator is hava minimal one
            if (count($super_admin) < 2) {
                return redirect('users')->with('error-alert', 'Harus ada minimal satu pengguna aktif sebagai administrator');
            }
        }

        // Validation
        $validator = $this->validateStore($request, true);

        // Return validation if fails
        if ($validator->fails()) {
            return redirect('users')->with('error', $validator->errors())->withInput();
        }

        // Get customer and details data
        $data = Users::find($id);

        // Store request data to object
        $data = $this->collectRequest($data, $request, $id);

        // Store data to databases
        $data->save();

        // Return success message
        return redirect('users')->with('success-alert', 'Data user berhasil di diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        // Get detail users
        $users = Users::find($id);

        // Check user type
        if ($users->user_type == 'Administrator') {

            // Check is super admin is more than one
            $super_admin = Users::where('user_type', '=', 'Administrator')
                    ->where('user_status', '=', 1)
                    ->get();

            // Check wheter adminstrator is hava minimal one
            if (count($super_admin) < 2) {
                return redirect('users')->with('error-alert', 'Harus ada minimal satu pengguna aktif sebagai administrator');
            }
        }

        // Check is users exist
        if ($users) {

            // Last delete users if exist
            $users->delete();

            // Return success into data page
            return redirect('users')->with('success-alert', 'Hapus data pengguna berhasil dilakukan');
        } else {

            // Return failed into data page
            return redirect('users')->with('error-alert', 'Hapus data pengguna gagal dilakukan');
        }
    }

    /**
     * Collect request and save to Properties Details object.
     *
     * @param  obj  $customerDetails
     * @param  request  $request
     * @return obj  $customerDetails
     */
    private function collectRequest($data, $request, $id = '') {

        // Switch is request create or update
        if ($id == '') {

            // Create data 
            $data = new Users;
            $data->user_id = Uuid::generate();
        } else {

            // Edit data
            $data = Users::find($id);
        }

        // Data collecting
        $data->user_fullname = $request->user_fullname;
        $data->user_email = $request->user_email;
        $data->user_username = $request->user_username;
        $data->user_nip = $request->user_nip;
        $data->user_type = $request->user_type;
        $data->user_status = $request->user_status;

        // Set password if user have input 
        if ($request->user_password)
            $data->user_password = Hash::make($request->user_password);

        // Return as object data
        return $data;
    }

    /**
     * Validate store customer request.
     *
     * @param  request  $request
     * @return obj  $customer
     */
    private function validateStore($request, $edit = false) {

        $value = [
            'user_fullname' => 'required|max:50',
            'user_email' => 'required|email|max:50',
            'user_username' => 'required|max:50',
            'user_type' => 'required',
            'user_nip' => 'required|max:20',
            'user_status' => 'required'
        ];

        if ($edit == false) {
            $value['user_password'] = 'required';
        }

        $validator = Validator::make($request->all(), $value);

        return $validator;
    }

}
