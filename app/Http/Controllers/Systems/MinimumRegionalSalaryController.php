<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\System\MinimumRegionalSalary;
use Validator;
use Uuid;

class MinimumRegionalSalaryController extends Controller {

    // Local private property
    private $base_url = 'minimum-regional-salary';
    private $object;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->object = new MinimumRegionalSalary();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        // Get data from database
        $send['data'] = $this->object->orderBy('mrs_year', 'asc')->paginate(10);

        // Return as index page
        return view('system.mrs.index', $send);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Create new object users
        $data = new $this->object;

        // Validation
        $validator = $this->validateStore($request);
        if ($validator->fails()) {
            return redirect($this->base_url)->with('error', $validator->errors())->withInput();
        }

        // Reset mrs_status into deactive
        if ($request->mrs_status == 1)
            $this->reset_status();

        // Collecting data
        $this->collectRequest($request);

        // Return data
        return redirect($this->base_url)->with('success-alert', 'Data upah minimum regional berhasil di tambahkan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        // Validation
        $validator = $this->validateStore($request, true);

        // Return validation if fails
        if ($validator->fails()) {
            return redirect($this->base_url)->with('error', $validator->errors())->withInput();
        }

        // Reset mrs_status into deactive
        if ($request->mrs_status == 1) {

            // Check mrs 
            $this->reset_status();
        } else {

            // Check data status
            if ($this->check_status($id) == false) {
                return redirect($this->base_url)->with('error-alert', 'Data UMR harus ada yang aktif');
            }
        }

        // Store request data to object
        $this->collectRequest($request, $id);

        // Return success message
        return redirect($this->base_url)->with('success-alert', 'Data peralatan berhasil di diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        // Get detail users
        $data = $this->object->find($id);

        // Check is users exist
        if ($data) {

            // Check data status
            if ($this->check_status($id) == false) {
                return redirect($this->base_url)->with('error-alert', 'Data UMR harus ada yang aktif');
            }

            // Last delete users if exist
            $data->delete();

            // Return success into data page
            return redirect($this->base_url)->with('success-alert', 'Hapus data peralatan berhasil dilakukan');
            
        } else {

            // Return failed into data page
            return redirect($this->base_url)->with('error-alert', 'Hapus data peralatan gagal dilakukan');
        }
    }

    /**
     * Collect request and save to minimum regional salary object.
     *
     * @param request  $request
     * @param uuid  $id
     * @return boolean  true
     */
    private function collectRequest($request, $id = '') {

        // Switch is request create or update
        if ($id == '') {

            // Create data
            $data = new $this->object;
            $data->mrs_id = Uuid::generate();
        } else {

            // Edit data
            $data = $this->object->find($id);
        }

        // Data collecting
        $data->mrs_year = $request->mrs_year;
        $data->mrs_nominal = $request->mrs_nominal;
        $data->mrs_status = $request->mrs_status;

        // Save data
        $data->save();

        // Return true
        return true;
    }

    /**
     * Validate store customer request.
     *
     * @param  request  $request
     * @return obj  $customer
     */
    private function validateStore($request, $edit = false) {

        // Array validation definition
        $value = [
            'mrs_year' => 'required|numeric|max:9999|min:2000',
            'mrs_nominal' => 'required|numeric|min:0',
            'mrs_status' => 'required'
        ];

        // Pass array validation defintion into validation class
        $validator = Validator::make($request->all(), $value);

        // Return as validation object
        return $validator;
    }

    /**
     * Function to reset status into deactive
     */
    private function reset_status() {

        // Reset into database 
        $this->object->where('mrs_status', '=', '1')
                ->update(['mrs_status' => '0']);

        // Return true
        return true;
    }

    /**
     * Function to reset status into deactive
     */
    private function check_status($id) {

        // Get total mrs status is exist or not
        $status_count = $this->object
                ->where('mrs_status', '=', 1)
                ->count();

        // Get last status deactive
        $status = $this->object->find($id);

        // Result 
        return ($status_count == 1 && $status->mrs_status == 1) ? false : true;
    }

}
