<?php

namespace App\Http\Controllers\Systems;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\System\Constructions;
use Illuminate\Support\Facades\DB;

class PdfPrintController extends Controller {

    /**
     * Get data for all construction report 
     *
     * @return App\Model\System\Constructions
     */
    public function all_constructions_report() {

        // Variable intialization for sql query
        $str = 'constructions.construction_id, '
                . ' constructions.construction_name,'
                . ' TotalEquipmentCost(constructions.construction_id) as equipment_cost,'
                . ' ROUND((TotalMaterialTime(constructions.construction_id)/60),2) as wpm,'
                . ' TotalManpowerCost(constructions.construction_id) as manpower_cost,'
                . ' ROUND('
                . ' (TotalEquipmentCost(constructions.construction_id) '
                . ' + TotalManpowerCost(constructions.construction_id)) * '
                . ' ROUND((TotalMaterialTime(constructions.construction_id)/60),2)'
                . ' ,2) as total_cost';

        // Get data from data base
        $send['data'] = Constructions::select(DB::raw($str))->get();

        // Get facade instance
        $pdf = \Barryvdh\DomPDF\Facade::class;

        // Load html paper and setup paper size and oriented
        $pdf = $pdf::loadView('pdf.constructions', $send)
                ->setPaper('a4', 'landscape');

        // Return pdf file 
        return $pdf->download('Daftar Biaya Konstruksi.pdf');
        
    }

    /**
     * Get data detail per construction report 
     *
     * @params id uuid
     * 
     * @return App\Model\System\Constructions
     */
    public function detail_constructions_report($id) {

        // Get data from data base
        $data = Constructions::select(DB::raw('*, ROUND('
                                . ' (TotalEquipmentCost(constructions.construction_id) '
                                . ' + TotalManpowerCost(constructions.construction_id)) * '
                                . ' ROUND((TotalMaterialTime(constructions.construction_id)/60),2)'
                                . ' ,2) as total_cost'))
                ->where('construction_id', '=', $id)
                ->first();

        // Load material details
        $data = $data->load('matpercons.materials');

        // Load equipment details
        $data = $data->load('equipercons.equipments');

        // Load manpowers details
        $data = $data->load('manpercons.manpowers');
        
        // Assign into passed variable
        $send['data'] = $data;

        // Get facade instance
        $pdf = \Barryvdh\DomPDF\Facade::class;

        // Load html paper and setup paper size and oriented
        $pdf = $pdf::loadView('pdf.constructions_detail', $send)
                ->setPaper('a4');

        // Return pdf file
        return $pdf->download('Detail Biaya Konstruksi.pdf');
        
    }

}
