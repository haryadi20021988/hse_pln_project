<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\Utility\ManpowerCalculation;
use App\Library\Utility\EquipmentCalc;
use Illuminate\Support\Facades\Input;

class CalcController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manpower_cost() {

        // Variable initialization
        $coeffiesien = Input::get('coeffisien');
        $per = Input::get('per');
        $round = Input::get('round') ? Input::get('round') : 1;
        $formated = Input::get('format');
        $result = null;

        // Get calulated manpower
        $manpower = new ManpowerCalculation();

        // Set coeffisien
        $manpower = $manpower->set($coeffiesien);

        // Select mode result
        if ($per == 'month') {
            $result = $manpower->month($round);
        } else {
            $result = $manpower->hour($round);
        }

        // Return result 
        return $formated == 'yes' ? number_format($result, $round, ',', '.') : $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function equipment_cost() {

        // Variable initialization
        $price = Input::get('price');
        $lifetime = Input::get('lifetime');
        $round = Input::get('round') ? Input::get('round') : 1;
        $formated = Input::get('format');

        // Get calculated equipment
        $equipcalc = new EquipmentCalc();

        // Set equipment
        $result = $equipcalc->init($price, $lifetime)->get($round);

        // Return result 
        return $formated == 'yes' ? number_format($result, $round, ',', '.') : $result;
        
    }

}
