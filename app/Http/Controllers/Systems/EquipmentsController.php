<?php

namespace App\Http\Controllers\Systems;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Uuid;
use App\Model\System\Equipments;
use Illuminate\Support\Facades\Input;

class EquipmentsController extends Controller {

    // Local private property
    private $base_url = 'equipments';
    private $object;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->object = new Equipments();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
         // Variable intialization
        $send['input_search'] = '';

        // Get data from database
        $data_temp = $this->object->select('*');
        
        // Do search if search keyword is available
        if (Input::get('search')) {
            
            // Build where query 
            $data_temp = $data_temp->where('equipment_type', 'like', '%' . Input::get('search') . '%');
            
            // Set up search key word
            $send['input_search'] = Input::get('search');
            
        }
        
        // Get data from database qery
        $send['data'] = $data_temp->orderBy('equipment_type','asc')->paginate(10);

        // Return as index page
        return view('system.equipments.index', $send);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Create new object users
        $data = new $this->object;

        // Validation
        $validator = $this->validateStore($request);
        if ($validator->fails()) {
            return redirect($this->base_url)->with('error', $validator->errors())->withInput();
        }

        // Collecting data
        $this->collectRequest($request);

        // Return data
        return redirect($this->base_url)->with('success-alert', 'Data peralatan berhasil di tambahkan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        // Validation
        $validator = $this->validateStore($request, true);

        // Return validation if fails
        if ($validator->fails()) {
            return redirect($this->base_url)->with('error', $validator->errors())->withInput();
        }

        // Store request data to object
        $this->collectRequest($request, $id);

        // Return success message
        return redirect($this->base_url)->with('success-alert', 'Data peralatan berhasil di diperbarui');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
         // Get detail users
        $data =  $this->object->find($id);

        // Check is users exist
        if ($data) {

            // Last delete users if exist
            $data->delete();

            // Return success into data page
            return redirect($this->base_url)->with('success-alert', 'Hapus data UMR berhasil dilakukan');
            
        } else {

            // Return failed into data page
            return redirect($this->base_url)->with('error-alert', 'Hapus data UMR gagal dilakukan');
            
        }
        
    }

    /**
     * Collect request and save to Properties Details object.
     *
     * @param  obj  $customerDetails
     * @param  request  $request
     * @return obj  $customerDetails
     */
    private function collectRequest($request, $id = '') {

        // Switch is request create or update
        if ($id == '') {

            // Create data
            $data = new $this->object;
            $data->equipment_id = Uuid::generate();
        } else {

            // Edit data
            $data = $this->object->find($id);
        }

        // Data collecting
        $data->equipment_type = $request->equipment_type;
        $data->equipment_price = $request->equipment_price;
        $data->equipment_lifetime = $request->equipment_lifetime;

        // Save data
        $data->save();

        // Return true
        return true;
        
    }

    /**
     * Validate store customer request.
     *
     * @param  request  $request
     * @return obj  $customer
     */
    private function validateStore($request, $edit = false) {

        // Array validation definition
        $value = [
            'equipment_type' => 'required|max:75',
            'equipment_price' => 'required|numeric|min:0',
            'equipment_lifetime' => 'required|numeric|max:50|min:0'
        ];

        // Pass array validation defintion into validation class
        $validator = Validator::make($request->all(), $value);

        // Return as validation object
        return $validator;
        
    }

}
