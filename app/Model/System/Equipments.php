<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Equipments extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'equipments';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'equipment_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the rooms record associated with the property.
     */
    public function equipercons() {
        return $this->hasMany('App\Model\System\Equipercons', 'equipercons_equipment_id', 'equipment_id');
    }

}
