<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Allowances extends Model {
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'allowances';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'allowance_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
}
