<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Manpercons extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'manpercons';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'manpercons_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the users record associated with the properties.
     */
    public function constructions() {
        return $this->belongsTo('App\Model\System\Constructions', 'manpercons_construction_id');
    }

    /**
     * Get the users record associated with the properties.
     */
    public function manpowers() {
        return $this->belongsTo('App\Model\System\Manpowers', 'manpercons_manpower_id')
                        ->select(DB::raw('*,manpowerhourlycost(manpower_coeffisien) as manpower_hourlycost'))
                        ->orderBy('manpower_qualification', 'asc');
    }

}
