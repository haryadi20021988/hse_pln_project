<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Manpowers extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'manpowers';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'manpower_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the rooms record associated with the property.
     */
    public function manpercons() {
        return $this->hasMany('App\Model\System\Manpercons', 'manpercons_manpower_id', 'manpower_id');
    }

    /**
     * Get the users record associated with the properties.
     */
    public function minimum_regional_salary() {
        return $this->belongsTo('App\Model\System\MinimumRegionalSalary', 'manpower_mrs_id');
    }
    
}
