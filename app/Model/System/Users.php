<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Users extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

}
