<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Equipercons extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'equipercons';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'equipercons_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the users record associated with the properties.
     */
    public function constructions() {
        return $this->belongsTo('App\Model\System\Constructions', 'equipercons_construction_id');
    }

    /**
     * Get the users record associated with the properties.
     */
    public function equipments() {
        return $this->belongsTo('App\Model\System\Equipments', 'equipercons_equipment_id')
                ->select(DB::raw('*,equipcost(equipment_price, equipment_lifetime) as equipment_cost'))
                ->orderBy('equipment_type', 'asc');
    }

}
