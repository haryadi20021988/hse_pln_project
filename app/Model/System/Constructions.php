<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Constructions extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'constructions';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'construction_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the rooms record associated with the property.
     */
    public function matpercons() {
        return $this->hasMany('App\Model\System\Matpercons', 'matpercons_construction_id', 'construction_id');
    }

    /**
     * Get the rooms record associated with the property.
     */
    public function manpercons() {
        return $this->hasMany('App\Model\System\Manpercons', 'manpercons_construction_id', 'construction_id');
    }

    /**
     * Get the rooms record associated with the property.
     */
    public function equipercons() {
        return $this->hasMany('App\Model\System\Equipercons', 'equipercons_construction_id', 'construction_id');
    }

}
