<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Materials extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'materials';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'material_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the rooms record associated with the property.
     */
    public function matpercons() {
        return $this->hasMany('App\Model\System\Matpercons', 'matpercons_material_id', 'material_id');
    }

}
