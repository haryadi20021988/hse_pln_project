<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Matpercons extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'matpercons';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'matpercons_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the users record associated with the properties.
     */
    public function constructions() {
        return $this->belongsTo('App\Model\System\Constructions', 'matpercons_construction_id');
    }
    
    /**
     * Get the users record associated with the properties.
     */
    public function materials() {
        return $this->belongsTo('App\Model\System\Materials', 'matpercons_material_id');
    }

}
