<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class MinimumRegionalSalary extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'minimum_regional_salary';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'mrs_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the rooms record associated with the property.
     */
    public function manpowers() {
        return $this->hasMany('App\Model\System\Manpowers', 'manpower_mrs_id', 'mrs_id');
    }

}
